
package fatherstransportregistrationprogram.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class WorkOrder implements Serializable {
    
    private Date timeStamp;
    private String from;
    private String to;
    private WorkType workType;
    private int km;
    private final int civPricePerKm=150;
    private final int comPricePerKm=160;
    private int price;

    public WorkOrder() {
        
    }
    
    
    
    public int getCivPricePerKm() {
        return civPricePerKm;
    }

    public int getComPricePerKm() {
        return comPricePerKm;
    }
    
    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public WorkType getWorkType() {
        return workType;
    }

    public void setWorkType(WorkType workType) {
        this.workType = workType;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.timeStamp);
        hash = 11 * hash + Objects.hashCode(this.from);
        hash = 11 * hash + Objects.hashCode(this.to);
        hash = 11 * hash + Objects.hashCode(this.workType);
        hash = 11 * hash + this.km;
        hash = 11 * hash + this.civPricePerKm;
        hash = 11 * hash + this.comPricePerKm;
        hash = 11 * hash + this.price;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkOrder other = (WorkOrder) obj;
        if (this.km != other.km) {
            return false;
        }
        if (this.civPricePerKm != other.civPricePerKm) {
            return false;
        }
        if (this.comPricePerKm != other.comPricePerKm) {
            return false;
        }
        if (this.price != other.price) {
            return false;
        }
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.timeStamp, other.timeStamp)) {
            return false;
        }
        if (this.workType != other.workType) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WorkOrder{" + "timeStamp=" + timeStamp + ", from=" + from + ", to=" + to + ", workType=" + workType + ", km=" + km + ", civPricePerKm=" + civPricePerKm + ", comPricePerKm=" + comPricePerKm + ", price=" + price + '}';
    }

    
    
    
    
}
