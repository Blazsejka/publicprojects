
package fatherstransportregistrationprogram.view;

import fatherstransportregistrationprogram.controller.TransportController;
import fatherstransportregistrationprogram.model.WorkOrder;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import static java.awt.GridBagConstraints.CENTER;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Gui {
    
    JFrame frame = new JFrame();
    protected TransportController transportController;
    protected WorkOrder workOrder;
    
    Font f = new Font("Courier New", Font.BOLD, 20);
    protected JPanel panel;
    protected JComboBox workTypeBox;
    protected JLabel workTypeLabel;
    public JTextField fromField;
    protected JLabel fromLabel;
    public JTextField toField;
    protected JLabel toLabel;
    public JTextField kmField;
    protected JLabel kmLabel;
    protected JLabel priceLabel1;
    public JLabel priceLabel2;
    protected JButton priceCountButton;
    protected JButton saveButton;
    protected JButton loadButton;
    protected JButton listButton;
    public JLabel listLabel;
    
    public Gui(TransportController transportController) {
        this.transportController=transportController;
        init();
    }
 
    
    private void init() {    
        setupWindow();
        setupWorkTypeBox();
        setupWorkTypeLabel();
        setupFromField();
        setupFromLabel();
        setupToField();
        setupToLabel();
        setupKmField();
        setupKmLabel();
        setupPriceLabel1();
        setupPriceLabel2();
        setupPriceCountButton();
        setupSaveButton();
        setupLoadButton();
        setupListButton();
        setupListLabel();
        frame.setVisible(true);
    }
  
       
    private void setupWindow() {
        frame.setSize(500,900);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Work order registration program");
        frame.setLayout(new GridLayout(1, 1));
        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        frame.add(panel);
        GridBagLayout gbl = new GridBagLayout();
        panel.setLayout(gbl);
        frame.setResizable(false);
    } 
    
    private void setupWorkTypeBox() {
        String[] workType = new String[] {"CIVILIANMOVING","CARGOTRANSPORTING"};
        this.workTypeBox = new JComboBox<String>(workType);
        this.workTypeBox.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(workTypeBox, gbc);
    }
    
    
    private void setupWorkTypeLabel() {
        GridBagConstraints gbc;
        this.workTypeLabel = new JLabel("Type of work:");
        this.workTypeLabel.setFont(f);
        workTypeLabel.setHorizontalAlignment(workTypeLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(workTypeLabel, gbc);                
    }
    
    
    private void setupFromField() {
        this.fromField = new JTextField("");
        this.fromField.setFont(f);
        fromField.setHorizontalAlignment(fromField.CENTER);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(fromField, gbc);
    }
    
    
    private void setupFromLabel() {
        GridBagConstraints gbc;
        this.fromLabel = new JLabel("Transport from:");
        this.fromLabel.setFont(f);
        fromLabel.setHorizontalAlignment(fromLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(fromLabel, gbc);                
    }
    
    
    private void setupToField() {
        this.toField = new JTextField("");
        this.toField.setFont(f);
        toField.setHorizontalAlignment(toField.CENTER);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(toField, gbc);
    }
    
    
    private void setupToLabel() {
        GridBagConstraints gbc;
        this.toLabel = new JLabel("Transport to:");
        this.toLabel.setFont(f);
        toLabel.setHorizontalAlignment(toLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(toLabel, gbc);                
    }
    
    
    private void setupKmField() {
        this.kmField = new JTextField("");
        this.kmField.setFont(f);
        kmField.setHorizontalAlignment(kmField.CENTER);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(kmField, gbc);
    }
    
    
    private void setupKmLabel() {
        GridBagConstraints gbc;
        this.kmLabel = new JLabel("Distance in km:");
        this.kmLabel.setFont(f);
        kmLabel.setHorizontalAlignment(kmLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(kmLabel, gbc);                
    }
    
    
    private void setupPriceLabel2() {
        this.priceLabel2 = new JLabel("0");
        this.priceLabel2.setFont(f);
        priceLabel2.setHorizontalAlignment(priceLabel2.CENTER);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(priceLabel2, gbc);
    }
    
    
    private void setupPriceLabel1() {
        GridBagConstraints gbc;
        this.priceLabel1 = new JLabel("Full price:");
        this.priceLabel1.setFont(f);
        priceLabel1.setHorizontalAlignment(priceLabel1.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(priceLabel1, gbc);                
    }
    
    
    private void setupPriceCountButton() {
        GridBagConstraints gbc;
        this.priceCountButton = new JButton("Count price");
        this.priceCountButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(priceCountButton, gbc);
        
        ActionListener countAction = c -> {    
            transportController.handleCountButtonClick();
        };
        priceCountButton.addActionListener(countAction);
    }
    
    
    private void setupSaveButton() {
        GridBagConstraints gbc;
        this.saveButton = new JButton("Save order");
        this.saveButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(saveButton, gbc);
        
        ActionListener saveAction = s -> {    
            try {
                transportController.handleSaveButtonClick();
            } catch (IOException ex) {
                Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        saveButton.addActionListener(saveAction);
        
    }
    
    private void setupLoadButton() {
        GridBagConstraints gbc;
        this.loadButton = new JButton("Load orders");
        this.loadButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(loadButton, gbc);
        
        ActionListener loadAction = l -> {    
            try {
                transportController.handleLoadButtonClick();
            } catch (IOException ex) {
                Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        };
        loadButton.addActionListener(loadAction);
        
    }
    
    private void setupListButton() {
        GridBagConstraints gbc;
        this.listButton = new JButton("List orders");
        this.listButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(listButton, gbc);
        
        ActionListener listAction = l -> {    
            transportController.handleListButtonClick();
            
        };
        listButton.addActionListener(listAction);
        
    }
    
    
    private void setupListLabel() {
        this.listLabel = new JLabel("");
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.weightx = 2;
        gbc.weighty = 2;
        gbc.gridwidth=2;
        gbc.gridheight=3;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(listLabel, gbc);
    }
    
    
}
