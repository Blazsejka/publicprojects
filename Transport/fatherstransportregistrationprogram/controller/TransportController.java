package fatherstransportregistrationprogram.controller;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import fatherstransportregistrationprogram.model.OrderRepository;
import fatherstransportregistrationprogram.model.WorkOrder;
import fatherstransportregistrationprogram.model.WorkType;
import fatherstransportregistrationprogram.view.Gui;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;

public class TransportController {

    OrderRepository orderRepository;
    Gui gui;
    WorkOrder tempOrder = new WorkOrder();
    String serPath = "orders.ser";
    WorkOrder tmp = new WorkOrder();

    public TransportController() {
        this.gui = new Gui(this);
    }

    public void handleCountButtonClick() {
        try {
            if (Integer.parseInt(gui.priceLabel2.getText()) != 0) {
                EnumSet<WorkType> types = EnumSet.allOf(WorkType.class);
                for (WorkType w : types) {
                    if (w.name().equals("CIVILIANMOVING")) {
                        gui.priceLabel2.setText(Integer.toString(2 * (Integer.parseInt(gui.kmField.getText()) * tempOrder.getCivPricePerKm())));
                    } else {
                        gui.priceLabel2.setText(Integer.toString(2 * (Integer.parseInt(gui.kmField.getText()) * tempOrder.getComPricePerKm())));
                    }
                }
            } else {
                MyException ex = new MyException();
                throw ex;
            }
        } catch (MyException ex) {
            gui.listLabel.setText(ex.getMessage());
        }

    }

    public void handleSaveButtonClick() throws FileNotFoundException, IOException {
        tmp.setFrom(gui.fromField.getText());
        tmp.setTo(gui.toField.getText());
        tmp.setKm(Integer.parseInt(gui.kmField.getText()));
        typer(tmp);
        timeStamper(tmp);
        pricer(tmp);
        OrderRepository.getInstance().orders.add(tmp);

        try (FileOutputStream fout = new FileOutputStream(serPath);
                ObjectOutputStream oout = new ObjectOutputStream(fout)) {
            oout.writeObject(OrderRepository.getInstance().orders);
        }

    }

    private void typer(WorkOrder tmp) {
        EnumSet<WorkType> types = EnumSet.allOf(WorkType.class);
        for (WorkType w : types) {
            if (w.name().equals("CIVILIANMOVING")) {
                tmp.setWorkType(WorkType.CIVILIANMOVING);
            } else {
                tmp.setWorkType(WorkType.CARGOTRANSPORTING);
            }
        }
    }

    private void pricer(WorkOrder tmp) throws NumberFormatException {
        EnumSet<WorkType> types = EnumSet.allOf(WorkType.class);
        for (WorkType w : types) {
            if (w.name().equals("CIVILIANMOVING")) {
                tmp.setPrice(2 * (Integer.parseInt(gui.kmField.getText()) * tempOrder.getCivPricePerKm()));
            } else {
                tmp.setPrice(2 * (Integer.parseInt(gui.kmField.getText()) * tempOrder.getComPricePerKm()));
            }

        }
    }

    private void timeStamper(WorkOrder tmp) {
        Date tempTimeStamp = new Date();
        long tempDate = tempTimeStamp.getTime();
        Date goodDate = new Date(tempDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yy/mm/dd");
        sdf.format(goodDate);
        tmp.setTimeStamp(goodDate);
    }

    public void handleLoadButtonClick() throws FileNotFoundException, IOException, ClassNotFoundException {
        try (FileInputStream fin = new FileInputStream(serPath);
                ObjectInputStream oin = new ObjectInputStream(fin)) {

            OrderRepository.getInstance().orders.add(oin.readObject());
        }

    }

    public void handleListButtonClick() {
        gui.listLabel.setText(OrderRepository.getInstance().orders.toString());

    }

}
