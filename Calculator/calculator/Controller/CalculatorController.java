
package calculator.Controller;

import calculator.Modell.CalculatorModell;
import calculator.Modell.Operand;
import calculator.View.CalculatorView;

public class CalculatorController {
    
    CalculatorModell calculatorModell;
    CalculatorView calculatorView;
    String temp = "";
    private double result=0;
    
    public CalculatorController() {
        calculatorModell = new CalculatorModell();
        calculatorView = new CalculatorView(this);
    }
    
        
    public void handlePlusButtonClick(double number) {
        calculatorModell.setNumber(number);
        temp="";
        calculatorView.screen.setText("+");
        calculatorModell.setOperand(Operand.Addition); 
    }
    
    public void handleMinusButtonClick(double number) {
        calculatorModell.setNumber(number);
        temp="";
        calculatorView.screen.setText("-");
        calculatorModell.setOperand(Operand.Extraction);
    }
    
    public void handleCButtonClick() {
        temp="";
        calculatorView.screen.setText("0");
        result=0;
    }
    
    public void handleMultiplyButtonClick(double number) {
        calculatorModell.setNumber(number);
        temp="";
        calculatorView.screen.setText("*");
        calculatorModell.setOperand(Operand.Multiply);
    }
    
    public void handleDivideButtonClick(double number) {
        calculatorModell.setNumber(number);
        temp="";
        calculatorView.screen.setText("/");
        calculatorModell.setOperand(Operand.Divide);
    }

    public void handleEqualsButtonClick(double number) {
        if (calculatorModell.getOperand().toString()=="Addition")
            result=calculatorModell.getNumber()+Double.parseDouble(calculatorView.screen.getText());
        
        if (calculatorModell.getOperand().toString()=="Extraction")
            result=calculatorModell.getNumber()-Double.parseDouble(calculatorView.screen.getText());
        
        if (calculatorModell.getOperand().toString()=="Multiply")
            result=calculatorModell.getNumber()*Double.parseDouble(calculatorView.screen.getText());
        
        if (calculatorModell.getOperand().toString()=="Divide")
            result=calculatorModell.getNumber()/Double.parseDouble(calculatorView.screen.getText());
        
        if (calculatorModell.getOperand().toString()==null)
            result = 0;
        
        calculatorView.screen.setText(Double.toString(result));
        calculatorModell.setNumber(0);
    } 
    
    public void handleNumberButtonClick(String number) {
        temp = temp + number;
        calculatorView.screen.setText(temp);
    }
    
}
