
package calculator.Modell;

public class CalculatorModell {
    
    private double number;
    private Operand operand;

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    public Operand getOperand() {
        return operand;
    }

    public void setOperand(Operand operand) {
        this.operand = operand;
    }
    
    
    
}
