
package calculator.View;

import calculator.Controller.CalculatorController;
import calculator.Modell.CalculatorModell;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class CalculatorView extends JFrame {
    
    public JTextField screen;
    private JPanel panel;
    private JButton plusButton;
    private JButton minusButton;
    private JButton equalsButton;
    private JButton cButton;
    private JButton multiplyButton;
    private JButton divideButton;
    private JButton[] numberButtons;
    
    CalculatorController calculatorController;
    
    public CalculatorView(CalculatorController calculatorController) {
        this.calculatorController = calculatorController;
        init();
           
    }
    
    
    public void init() {
        
        setUpWindow();
        setUpCalculatorScreen();
        setupPlusButton();
        setupMinusButton();
        setupEqualsButton();
        setupCButton();
        setupMultiplyButton();
        setupDivideButton();
        setupNumberButtons();
        this.setVisible(true);
        
        
    }
    
        
       
    private void setUpWindow() {
        this.setSize(600,600);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Balage's calculator");

        setLayout(new GridLayout(1, 1));

        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        add(panel);

        GridBagLayout gbl = new GridBagLayout();
        panel.setLayout(gbl);
    }
    
    private void setUpCalculatorScreen() {
        this.screen = new JTextField("0");
        Font f = new Font("Lucida Console", Font.BOLD, 30);
        this.screen.setHorizontalAlignment(JTextField.RIGHT);
        this.screen.setFont(f);
        screen.setEditable(false);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 13;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(screen, gbc);
    }
    
    private void setupPlusButton() {
        GridBagConstraints gbc;
        this.plusButton = new JButton("+");
        Font f = new Font("Lucida Console", Font.BOLD, 20);
        this.plusButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(plusButton, gbc);
        
        ActionListener plusAction = p -> {
            
            calculatorController.handlePlusButtonClick(Double.valueOf(screen.getText()));
        
        };
        plusButton.addActionListener(plusAction);
        
        
        
    }

    private void setupMinusButton() {
        GridBagConstraints gbc;
        this.minusButton = new JButton("-");
        Font f = new Font("Lucida Console", Font.BOLD, 20);
        this.minusButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(minusButton, gbc);
        
        ActionListener minusAction = m -> {
            
            calculatorController.handleMinusButtonClick(Double.valueOf(screen.getText()));
        
        };
        minusButton.addActionListener(minusAction);
        
    }
    
    private void setupEqualsButton() {
        GridBagConstraints gbc;
        this.equalsButton = new JButton("=");
        Font f = new Font("Lucida Console", Font.BOLD, 20);
        this.equalsButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(equalsButton, gbc);
        
        ActionListener equalsAction = e -> {
            calculatorController.handleEqualsButtonClick(Double.valueOf(screen.getText()));
        };
        equalsButton.addActionListener(equalsAction);
        
    }
    
    
    private void setupCButton() {
        GridBagConstraints gbc;
        this.cButton = new JButton("C");
        Font f = new Font("Lucida Console", Font.BOLD, 20);
        this.cButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(cButton, gbc);
        
        ActionListener CAction = c -> {
            calculatorController.handleCButtonClick();
        };
        cButton.addActionListener(CAction);
        
    }
    
    private void setupMultiplyButton() {
        GridBagConstraints gbc;
        this.multiplyButton = new JButton("*");
        Font f = new Font("Lucida Console", Font.BOLD, 20);
        this.multiplyButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(multiplyButton, gbc);
        
        ActionListener multiplyAction = m -> {
            calculatorController.handleMultiplyButtonClick(Double.valueOf(screen.getText()));
        };
        multiplyButton.addActionListener(multiplyAction);
        
    }
    
    private void setupDivideButton() {
        GridBagConstraints gbc;
        this.divideButton = new JButton("/");
        Font f = new Font("Lucida Console", Font.BOLD, 20);
        this.divideButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(divideButton, gbc);
        
        ActionListener divideAction = d -> {
            calculatorController.handleDivideButtonClick(Double.valueOf(screen.getText()));
        };
        divideButton.addActionListener(divideAction);
        
    }
    
    
    private void setupNumberButtons() {
        
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        numberButtons = new JButton[10];
        Font f = new Font("Lucida Console", Font.BOLD, 20);
        
        
            numberButtons[0] = new JButton(String.valueOf(0));
            this.numberButtons[0].setFont(f);
            gbc.gridx = 1;
            gbc.gridy = 6;
            gbc.weightx = 1;
            gbc.weighty = 1;
            gbc.gridwidth=3;
            gbc.fill = GridBagConstraints.BOTH;
            panel.add(numberButtons[0], gbc);
            ActionListener number0Action = n -> {
                calculatorController.handleNumberButtonClick(numberButtons[0].getText().toString());
            };
            numberButtons[0].addActionListener(number0Action);
            
            numberButtons[1] = new JButton(String.valueOf(1));
            this.numberButtons[1].setFont(f);
            gbc.gridwidth=1;
            gbc.gridx = 1;
            gbc.gridy = 3;
            gbc.weightx = 1;
            gbc.weighty = 1;
            gbc.fill = GridBagConstraints.BOTH;
            panel.add(numberButtons[1], gbc);
            ActionListener number1Action = n -> {
                calculatorController.handleNumberButtonClick(numberButtons[1].getText().toString());
            };
            numberButtons[1].addActionListener(number1Action);
            
            numberButtons[2] = new JButton(String.valueOf(2));
            this.numberButtons[2].setFont(f);
            gbc.gridx = 2;
            gbc.gridy = 3;
            gbc.weightx = 1;
            gbc.weighty = 1;
            gbc.fill = GridBagConstraints.BOTH;
            panel.add(numberButtons[2], gbc);
            ActionListener number2Action = n -> {
                calculatorController.handleNumberButtonClick(numberButtons[2].getText().toString());
            };
            numberButtons[2].addActionListener(number2Action);
            
            numberButtons[3] = new JButton(String.valueOf(3));
            this.numberButtons[3].setFont(f);
            gbc.gridx = 3;
            gbc.gridy = 3;
            gbc.weightx = 1;
            gbc.weighty = 1;
            gbc.fill = GridBagConstraints.BOTH;
            panel.add(numberButtons[3], gbc);
            ActionListener number3Action = n -> {
                calculatorController.handleNumberButtonClick(numberButtons[3].getText().toString());
            };
            numberButtons[3].addActionListener(number3Action);
            
            numberButtons[4] = new JButton(String.valueOf(4));
            this.numberButtons[4].setFont(f);
            gbc.gridx = 1;
            gbc.gridy = 4;
            gbc.weightx = 1;
            gbc.weighty = 1;
            gbc.fill = GridBagConstraints.BOTH;
            panel.add(numberButtons[4], gbc);
            ActionListener number4Action = n -> {
                calculatorController.handleNumberButtonClick(numberButtons[4].getText().toString());
            };
            numberButtons[4].addActionListener(number4Action);
            
            numberButtons[5] = new JButton(String.valueOf(5));
            this.numberButtons[5].setFont(f);
            gbc.gridx = 2;
            gbc.gridy = 4;
            gbc.weightx = 1;
            gbc.weighty = 1;
            gbc.fill = GridBagConstraints.BOTH;
            panel.add(numberButtons[5], gbc);
            ActionListener number5Action = n -> {
                calculatorController.handleNumberButtonClick(numberButtons[5].getText().toString());
            };
            numberButtons[5].addActionListener(number5Action);
            
            numberButtons[6] = new JButton(String.valueOf(6));
            this.numberButtons[6].setFont(f);
            gbc.gridx = 3;
            gbc.gridy = 4;
            gbc.weightx = 1;
            gbc.weighty = 1;
            gbc.fill = GridBagConstraints.BOTH;
            panel.add(numberButtons[6], gbc);
            ActionListener number6Action = n -> {
                calculatorController.handleNumberButtonClick(numberButtons[6].getText().toString());
            };
            numberButtons[6].addActionListener(number6Action);
            
            numberButtons[7] = new JButton(String.valueOf(7));
            this.numberButtons[7].setFont(f);
            gbc.gridx = 1;
            gbc.gridy = 5;
            gbc.weightx = 1;
            gbc.weighty = 1;
            gbc.fill = GridBagConstraints.BOTH;
            panel.add(numberButtons[7], gbc);
            ActionListener number7Action = n -> {
                calculatorController.handleNumberButtonClick(numberButtons[7].getText().toString());
            };
            numberButtons[7].addActionListener(number7Action);
            
            numberButtons[8] = new JButton(String.valueOf(8));
            this.numberButtons[8].setFont(f);
            gbc.gridx = 2;
            gbc.gridy = 5;
            gbc.weightx = 1;
            gbc.weighty = 1;
            gbc.fill = GridBagConstraints.BOTH;
            panel.add(numberButtons[8], gbc);
            ActionListener number8Action = n -> {
                calculatorController.handleNumberButtonClick(numberButtons[8].getText().toString());
            };
            numberButtons[8].addActionListener(number8Action);
            
            numberButtons[9] = new JButton(String.valueOf(9));
            this.numberButtons[9].setFont(f);
            gbc.gridx = 3;
            gbc.gridy = 5;
            gbc.weightx = 1;
            gbc.weighty = 1;
            gbc.fill = GridBagConstraints.BOTH;
            panel.add(numberButtons[9], gbc);
            ActionListener number9Action = n -> {
                calculatorController.handleNumberButtonClick(numberButtons[9].getText().toString());
            };
            numberButtons[9].addActionListener(number9Action);
            
            
   
        
    }
    
    

    
}
