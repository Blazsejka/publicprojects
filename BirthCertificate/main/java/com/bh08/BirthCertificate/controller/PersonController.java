package com.bh08.BirthCertificate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.bh08.BirthCertificate.model.Person;
import com.bh08.BirthCertificate.service.PersonService;

@Controller
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	
	public void takeOnNewPerson(Person person) {
		personService.saveNewPerson(person);
	}
	
	public List<Person> writeOutAllPerson() {
		return personService.getAllPerson();
	}
	
}
