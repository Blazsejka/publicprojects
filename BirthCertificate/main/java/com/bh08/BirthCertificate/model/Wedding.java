package com.bh08.BirthCertificate.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Wedding extends Event {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToOne
	private CivilWedding civilWedding;
	
	@OneToOne
	private EcclesiasticalWedding ecclesiasticalWedding;
	
	@OneToOne
	private Divorce divorce;
	
	@ManyToOne
	private Person person;
	
}
