package com.bh08.BirthCertificate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.bh08.BirthCertificate.controller.PersonController;
import com.bh08.BirthCertificate.model.Birth;
import com.bh08.BirthCertificate.model.Gender;
import com.bh08.BirthCertificate.model.Person;

@SpringBootApplication
public class BirthCertificateApplication {
	
//	@Autowired
//	private PersonController personController;
	
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(new Class<?>[] {BirthCertificateApplication.class, BirthCertificateConfig.class}, args);
		
		PersonController personController = context.getBean("personController",PersonController.class);
		
		Person person = new Person();
		person.setGender(Gender.FEMALE);
		person.setName("Ica");
		
		personController.takeOnNewPerson(person);
		
		System.out.println(personController.writeOutAllPerson());
		
		
	}

}
