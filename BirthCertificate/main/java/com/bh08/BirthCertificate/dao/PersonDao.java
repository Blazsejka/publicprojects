package com.bh08.BirthCertificate.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bh08.BirthCertificate.model.Person;

@Repository
public interface PersonDao extends JpaRepository<Person, Long> {
	
	public List<Person> findAll();
	
	public Optional<Person> findById(Long id);
	
	
	
}
