package com.bh08.BirthCertificate;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = "com.bh08.BirthCertificate")
@EnableJpaRepositories(basePackages = "com.bh08.BirthCertificate")
@ComponentScan(basePackages = "com.bh08.BirthCertificate")
@Configuration
public class BirthCertificateConfig {
	
	
}
