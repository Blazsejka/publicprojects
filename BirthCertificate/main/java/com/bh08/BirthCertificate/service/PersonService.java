package com.bh08.BirthCertificate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.BirthCertificate.dao.PersonDao;
import com.bh08.BirthCertificate.model.Person;

@Service
public class PersonService {
	
	@Autowired
	private PersonDao personDao;
	
	
	public void saveNewPerson(Person person) {
		personDao.saveAndFlush(person);
	}
	
	
	public List<Person> getAllPerson() {
		return personDao.findAll();
	}
	
	
}
