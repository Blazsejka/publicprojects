package view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.Repository;

@WebServlet("/ResultServlet")
public class ResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//Repository repository = new Repository();

	public ResultServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int goodAnswerCounter = 0;

		for (int i = 1; i < 22; i++) {
			System.out.print(request.getParameter("answer" + i) + " --- ");
			System.out.print(Repository.quizes.get(i-1).getSolution());
			System.out.println();
			if (((String) request.getParameter("answer" + i)).equals(Repository.quizes.get(i-1).getSolution())) {
				goodAnswerCounter++;
			}
		}

		request.setAttribute("goodAnswers", goodAnswerCounter);
		request.getRequestDispatcher("/Result.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
