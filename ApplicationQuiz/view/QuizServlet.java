package view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DBReader;
import service.Repository;
import service.Service;
@WebServlet("/QuizServlet")

public class QuizServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private Service service = new Service();
	//private Repository repository = new Repository();
	private DBReader dbreader = new DBReader(service);
	
	public QuizServlet() throws SQLException {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		
		request.setAttribute("questions", Repository.getQuizes());
		request.getRequestDispatcher("/InterviewQuiz.jsp").forward(request, response);
		

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
