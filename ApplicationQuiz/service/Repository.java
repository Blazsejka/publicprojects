package service;

import java.util.ArrayList;
import java.util.List;

import model.Quiz;

public abstract class Repository {
	
	public static List<Quiz> quizes = new ArrayList<>();
	
	public Repository() {
	
	}
	
	public static void setQuizes(List<Quiz> quizes) {
		Repository.quizes = quizes;
	}
	
	public static List<Quiz> getQuizes() {
		return quizes;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	

}
