package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Quiz;
import service.Repository;
import service.Service;

public class DBReader {
	
	private Quiz quiz;
	private Service service;
	//private Repository repository;
	
	public DBReader(Service service) throws SQLException {
		this.service=service;
		
	}
	
	
	public void readQuestions() throws SQLException {
		//Repository repository = new Repository();
		Quiz tmpquiz = null;
        try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "12345c")) {
            String selectSQL = "SELECT * FROM INTERVIEWQUIZ";
            PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
            	tmpquiz = new Quiz();
                tmpquiz.setQuestion(rs.getString("QUESTION"));
                tmpquiz.setAnswer1(rs.getString("ANSWER1"));
                tmpquiz.setAnswer2(rs.getString("ANSWER2"));
                tmpquiz.setAnswer3(rs.getString("ANSWER3"));
                tmpquiz.setSolution(rs.getString("SOLUTION"));
                Repository.quizes.add(tmpquiz);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

	

}
