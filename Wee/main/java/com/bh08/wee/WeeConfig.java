package com.bh08.wee;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = "com.bh08.wee.model")
@EnableJpaRepositories(basePackages = "com.bh08.wee.daos")
@ComponentScan(basePackages = "com.bh08.wee")
@Configuration
public class WeeConfig {

}
