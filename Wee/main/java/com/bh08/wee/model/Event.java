package com.bh08.wee.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Event implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long eventId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn
	private User eventCreator;

	private LocalDateTime eventFrom;

	private LocalDateTime eventUntil;

	@Column(nullable = false)
	private String eventTitle;

	private String eventLocation;

	private String eventDescription;

//    @OneToMany(fetch = FetchType.LAZY, mappedBy="inviteId")
//	private List<EventInvite> eventInvites = new ArrayList<>();

}
