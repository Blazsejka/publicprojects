package com.bh08.wee.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import com.bh08.wee.model.Department;
import com.bh08.wee.model.Gender;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "Login")
@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;

	@Column(unique = true, nullable = false)
	@Email
	private String userEmail;

	@Column(nullable = false)
	private String userPassword;

	private String userName;
	private String userBirthPlace;
	private LocalDate userBirthDate;

	@Enumerated(EnumType.STRING)
	private Gender userGender;
	
	private String userCity;
	
	@Column(unique = true)
	private String userProfilePic;

	@ManyToOne
	private Department userDepartment;

	private String userSchools;

	private String userWorkplaces;

	public User(String userEmail, String userPassword, String userName, String userBirthPlace, LocalDate userBirthDate,
			Gender userGender, String userPlace, String userProfilePic, Department userDepartment, String userSchools,
			String userWorkplaces) {
		super();
		this.userEmail = userEmail;
		this.userPassword = userPassword;
		this.userName = userName;
		this.userBirthPlace = userBirthPlace;
		this.userBirthDate = userBirthDate;
		this.userGender = userGender;
		this.userCity = userPlace;
		this.userProfilePic = userProfilePic;
		this.userDepartment = userDepartment;
		this.userSchools = userSchools;
		this.userWorkplaces = userWorkplaces;
	}

}
