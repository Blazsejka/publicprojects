package com.bh08.wee.viewmodel.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.wee.model.Event;
import com.bh08.wee.model.EventInvite;
import com.bh08.wee.service.EventInviteService;
import com.bh08.wee.service.EventService;
import com.bh08.wee.viewmodel.dtos.eventinvite.ExtendedEventInviteDTO;
import com.bh08.wee.viewmodel.dtos.eventinvite.ExtendedEventInviteListDTO;

@Service
public class ExtendedEventInviteDataService {
	@Autowired
	private EventInviteService eventInviteService;

	public ExtendedEventInviteListDTO getEventInvitesDto(Event event) {
		List<EventInvite> eventInviteList = eventInviteService.getInvitesForEvent(event);
		List<ExtendedEventInviteDTO> inviteListDto = new ArrayList<>();
		for (EventInvite eventInvite : eventInviteList) {
			inviteListDto.add(getEventInviteExtendedDto(eventInvite));
		}
		return getEventInvitesDto(inviteListDto);
	}

	public ExtendedEventInviteDTO getEventInviteExtendedDto(EventInvite eventInvite) {
		ExtendedEventInviteDTO eventInviteDto = new ExtendedEventInviteDTO();
		eventInviteDto.setUserEmail(eventInvite.getInvited().getUserEmail());
		eventInviteDto.setUserName(eventInvite.getInvited().getUserName());
		eventInviteDto.setEventInviteId(eventInvite.getInviteId().toString());
		if (eventInvite.isInviteValid()) {
			eventInviteDto.setInviteDeclined("false");
		} else {
			eventInviteDto.setInviteDeclined("true");
		}
		return eventInviteDto;
	}

	private ExtendedEventInviteListDTO getEventInvitesDto(List<ExtendedEventInviteDTO> eventInvites) {
		ExtendedEventInviteListDTO eventInvitesDto = new ExtendedEventInviteListDTO();
		for (ExtendedEventInviteDTO eventInvite : eventInvites) {
			eventInvitesDto.addEventInvite(eventInvite);
		}
		return eventInvitesDto;
	}
	

}
