package com.bh08.wee.viewmodel.service;

import java.time.LocalDateTime;
import com.bh08.wee.model.Event;
import com.bh08.wee.viewmodel.dtos.event.EventDTOInterface;

public interface EventsDataService<E extends EventDTOInterface> {

	public default E getEventData(Event event, E eventData) {
		eventData.setEventId(event.getEventId());
		eventData.setEventTitle(event.getEventTitle());
		eventData.setEventDescription(event.getEventDescription());
		eventData.setEventLocation(event.getEventLocation());
		LocalDateTime eventFrom = event.getEventFrom();
		System.out.println("Event from" + eventFrom);
		LocalDateTime eventUntil = event.getEventUntil();
		System.out.println("Event until" + eventFrom);
		eventData.setEventFromDate(eventFrom.toLocalDate());
		eventData.setEventFrom(eventFrom);
		eventData.setEventUntil(eventUntil);
		eventData.setEventFromYear(Integer.toString(eventFrom.getYear()));
		eventData.setEventFromMonth(Integer.toString(eventFrom.getMonthValue()));
		eventData.setEventFromDay(Integer.toString(eventFrom.getDayOfMonth()));
		eventData.setEventFromHour(Integer.toString(eventFrom.getHour()));
		eventData.setEventFromMinutes(Integer.toString(eventFrom.getMinute()));
		eventData.setEventUntilHour(Integer.toString(eventUntil.getHour()));
		eventData.setEventUntilMinutes(Integer.toString(eventUntil.getMinute()));
		return eventData;
	}

}
