package com.bh08.wee.viewmodel.service;

import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.wee.model.Event;
import com.bh08.wee.model.User;
import com.bh08.wee.viewmodel.data.event.NewEventFormData;

@Service
public class NewEventFormDataService {

	public Event convertToEvent(User user, NewEventFormData eventFormData) {
		Event event = new Event();
		event.setEventCreator(user);
		event.setEventTitle(eventFormData.getEventTitle());
		event.setEventLocation(eventFormData.getEventLocation());
		event.setEventDescription(eventFormData.getEventDescription());
		LocalDateTime eventFrom = getFromDateTime(eventFormData);
		LocalDateTime eventUntil = getUntilDateTime(eventFormData);
		event.setEventFrom(eventFrom);
		event.setEventUntil(eventUntil);
		return event;
	}

	public LocalDateTime getFromDateTime(NewEventFormData eventFormData) {
		Integer eventFromMinutes = Integer.parseInt(eventFormData.getEventFromMinute());
		return LocalDateTime.of(eventFormData.getEventFromYear(), eventFormData.getEventFromMonth(),
				eventFormData.getEventFromDay(), eventFormData.getEventFromHour(), eventFromMinutes);
	}

	public LocalDateTime getUntilDateTime(NewEventFormData eventFormData) {
		Integer eventUntilMinutes = Integer.parseInt(eventFormData.getEventUntilMinute());
		return LocalDateTime.of(eventFormData.getEventFromYear(), eventFormData.getEventFromMonth(),
				eventFormData.getEventFromDay(), eventFormData.getEventUntilHour(), eventUntilMinutes);
	}

	public LocalTime getFromTime(NewEventFormData eventFormData) {
		Integer eventFromMinutes = Integer.parseInt(eventFormData.getEventFromMinute());
		return LocalTime.of(eventFormData.getEventFromHour(), eventFromMinutes);
	}

	public LocalTime getUntilTime(NewEventFormData eventFormData) {
		Integer eventUntilMinutes = Integer.parseInt(eventFormData.getEventUntilMinute());
		return LocalTime.of(eventFormData.getEventUntilHour(), eventUntilMinutes);
	}

	public boolean hasEmptyField(NewEventFormData eventFormData) {
		if (null == eventFormData.getEventFromYear() || null == eventFormData.getEventFromMonth()
				|| null == eventFormData.getEventFromDay() || null == eventFormData.getEventFromHour()
				|| null == eventFormData.getEventUntilHour() || null == eventFormData.getEventFromMinute()
				|| null == eventFormData.getEventUntilMinute()) {
			return true;
		}
		return false;
	}
}
