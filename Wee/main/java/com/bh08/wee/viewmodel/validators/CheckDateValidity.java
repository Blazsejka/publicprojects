package com.bh08.wee.viewmodel.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = CheckDateValidityValidator.class)
public @interface CheckDateValidity {
	String message();

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
