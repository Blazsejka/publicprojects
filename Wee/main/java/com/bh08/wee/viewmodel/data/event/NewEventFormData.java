package com.bh08.wee.viewmodel.data.event;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bh08.wee.viewmodel.validators.CheckDateValidity;
import com.bh08.wee.viewmodel.validators.CheckTemporalPrecedence;

import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
@CheckDateValidity(message = "Incorrect date")
@CheckTemporalPrecedence(message = "End of the event should be after start of the event")
public class NewEventFormData {

	@NotEmpty
	@Size(min = 1, max = 255)
	private String eventTitle;

	@NotEmpty
	@Size(min = 1, max = 1000)
	private String eventDescription;

	@NotEmpty
	@Size(min = 1, max = 255)
	private String eventLocation;

//	@NotNull(message = "Field cannot be empty")
	@Min(value = 2019)
	@Max(value = 2021)
	@Digits(integer = 4, fraction = 0)
	private Integer eventFromYear;

//	@NotNull(message = "Field cannot be empty")
	@Min(value = 1)
	@Max(value = 12)
	@Digits(integer = 2, fraction = 0)
	private Integer eventFromMonth;

//	@NotNull(message = "Field cannot be empty")
	@Min(value = 1)
	@Max(value = 31)
	@Digits(integer = 2, fraction = 0)
	private Integer eventFromDay;

//	@NotNull(message = "Field cannot be empty")
	@Min(value = 0)
	@Max(value = 24)
	@Digits(integer = 2, fraction = 0)
	private Integer eventFromHour;

	private String eventFromMinute;

//	@NotNull(message = "Field cannot be empty")
	@Min(value = 0)
	@Max(value = 24)
	@Digits(integer = 2, fraction = 0)
	private Integer eventUntilHour;

	private String eventUntilMinute;

	public String getEventTitle() {
		return eventTitle;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public String getEventLocation() {
		return eventLocation;
	}

	@NotNull(message = "Field cannot be empty")
	public Integer getEventFromYear() {
		return eventFromYear;
	}

	@NotNull(message = "Field cannot be empty")
	public Integer getEventFromMonth() {
		return eventFromMonth;
	}

	@NotNull(message = "Field cannot be empty")
	public Integer getEventFromDay() {
		return eventFromDay;
	}

	@NotNull(message = "Field cannot be empty")
	public Integer getEventFromHour() {
		return eventFromHour;
	}

	@NotNull(message = "Field cannot be empty")
	public String getEventFromMinute() {
		return eventFromMinute;
	}

	@NotNull(message = "Field cannot be empty")
	public Integer getEventUntilHour() {
		return eventUntilHour;
	}

	@NotNull(message = "Field cannot be empty")
	public String getEventUntilMinute() {
		return eventUntilMinute;
	}

}
