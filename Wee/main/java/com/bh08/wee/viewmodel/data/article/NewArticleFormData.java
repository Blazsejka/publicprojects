package com.bh08.wee.viewmodel.data.article;

import java.sql.Date;

import javax.validation.constraints.NotEmpty;

import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
public class NewArticleFormData {

	@NotEmpty
	private String newsAvailableUntil;
	
	@NotEmpty
	private String newsTitle;

	@NotEmpty
	private String newsText;

	public String getNewsAvailableUntil() {
		return newsAvailableUntil;
	}

	public String getNewsTitle() {
		return newsTitle;
	}

	public String getNewsText() {
		return newsText;
	}
	
	
}
