package com.bh08.wee.viewmodel.dtos.eventinvite;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.bh08.wee.viewmodel.dtos.event.EventByUserDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EventInviteWithEventForUserDTO extends EventByUserDTO {
	private String eventInviteId;
	private String invitationDeclined;
	private String eventCreatorName;
	private String eventCreatorEmail;
}
