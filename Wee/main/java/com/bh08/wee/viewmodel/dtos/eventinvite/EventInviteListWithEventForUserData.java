package com.bh08.wee.viewmodel.dtos.eventinvite;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EventInviteListWithEventForUserData {
	private List<EventInviteWithEventForUserDTO> usersInvitesList = new ArrayList<>();

	public void addInvite(EventInviteWithEventForUserDTO invite) {
		this.usersInvitesList.add(invite);
	}
}
