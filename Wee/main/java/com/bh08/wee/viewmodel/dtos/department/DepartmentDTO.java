package com.bh08.wee.viewmodel.dtos.department;

import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class DepartmentDTO {
	private String departmentId;
	private String departmentName;
}
