package com.bh08.wee.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.bh08.wee.model.Department;

@Repository
public interface DepartmentDAO extends JpaRepository<Department, Long> {
	public List<Department> findAll();

	public Optional<Department> findById(Long departmentId);
	
	public Optional<Department> findByDepartmentName(String departmentName);

}
