package com.bh08.wee.daos;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bh08.wee.model.Event;
import com.bh08.wee.model.User;

@Repository
public interface EventDAO extends JpaRepository<Event, Long> {
	public List<Event> findAll();

	public Optional<Event> findById(Long eventId);

	public List<Event> findByEventFromAfterOrderByEventFromDesc(LocalDateTime date);

	public List<Event> findByEventFromBeforeOrderByEventFromDesc(LocalDateTime date);

	public List<Event> findByEventCreatorOrderByEventFromDesc(User user);

	public List<Event> findByEventCreatorAndEventFromAfterOrderByEventFromDesc(User user, LocalDateTime date);

	public List<Event> findByEventCreatorAndEventFromBeforeOrderByEventFromDesc(User user, LocalDateTime date);

}
