package com.bh08.wee.daos;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bh08.wee.model.News;

public interface ArticleDAO extends JpaRepository<News, Long> {

	public List<News> findAll();
	
	public List<News> findByNewsAvailableUntilAfterOrderByNewsAvailableUntilAsc(LocalDate date);

}
