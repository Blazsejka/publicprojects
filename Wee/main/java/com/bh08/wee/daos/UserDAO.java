package com.bh08.wee.daos;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.bh08.wee.model.Department;
import com.bh08.wee.model.Gender;
import com.bh08.wee.model.User;

@Repository
@RepositoryRestResource(collectionResourceRel = "usersearch", path = "usersearch")
public interface UserDAO extends JpaRepository<User, Long> {

	public List<User> findAll();
	
	public Optional<User> findByUserEmail(String userEmail);	
	//public User findByUserEmail(String userEmail);
	
	public Optional<User> findByUserName(String userName);	

	public List<User> findByUserDepartment(Department department);

	public List<User> findByUserGender(Gender gender);

	public List<User> findByUserCity(String city);

	public List<User> findByUserCityContaining(String citySearchExpression);

	public List<User> findByUserDepartmentAndUserNameContaining(Department department, String nameSearchExpression);

	public List<User> findByUserId(@Param("id") Long userId);

	public List<User> findByUserNameContaining(@Param("name") String name);

	@Query(value = "SELECT u FROM User u JOIN u.userDepartment d ON d.departmentId = :department")
	List<User> findByDepartmentId(@Param("department") Long department);

	@Query(value = "select u from User u where u.userEmail = ?1")
	User findByEmailAddress(@Param("email") String emailAddress);

	@Query(value = "select u from User u JOIN u.userDepartment d where d.departmentId= :department and u.userName LIKE %:name%")
	List<User> findByDepartmentAndName(@Param("department") Long department, @Param("name") String name);

}
