package com.bh08.wee.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.wee.daos.EventDAO;
import com.bh08.wee.daos.EventInviteDAO;
import com.bh08.wee.exceptions.EventInviteNotFoundException;
import com.bh08.wee.model.Event;
import com.bh08.wee.model.EventInvite;
import com.bh08.wee.model.User;

@Service
public class EventInviteService {
	@Autowired
	EventInviteDAO eventInviteDao;
	@Autowired
	EventDAO eventDao;

	public EventInvite saveEventInvite(EventInvite eventInvite) {
		return eventInviteDao.saveAndFlush(eventInvite);
	}

	public EventInvite saveDeclinedEventInvite(Long eventInviteId) throws EventInviteNotFoundException {
		Optional<EventInvite> eventInvite = eventInviteDao.findById(eventInviteId);
		if (eventInvite.isPresent()) {
			EventInvite declinedEventInvite = eventInvite.get();
			declinedEventInvite.setInviteValid(false);
			return eventInviteDao.saveAndFlush(declinedEventInvite);
		} else {
			throw new EventInviteNotFoundException();
		}
	}

	public List<EventInvite> getAllEventInvites() {
		return eventInviteDao.findAll();
	}

	public List<EventInvite> getValidInvitesForEvent(Event event) {
		return eventInviteDao.findByEventAndInviteValid(event, true);
	}

	public List<EventInvite> getInvitesForEvent(Event event) {
		return eventInviteDao.findByEvent(event);
	}

	public List<EventInvite> getInvitesForUser(User user) {
		return eventInviteDao.findByInvitedOrderByInviteCreatedAsc(user);
	}

//	public List<EventInvite> getFutureInvitesForUser(User user){
//		return eventInviteDao.findByInvitedAndInviteCreatedAfterOrderByInviteCreatedAsc(user, new Date());
//	}

	public List<EventInvite> getFutureInvites() {
		List<Event> events = eventDao.findByEventFromBeforeOrderByEventFromDesc(LocalDateTime.now());
		return eventInviteDao.findByEventIn(events);
	}

	public List<EventInvite> getFutureInvitesForUser(User user) {
		List<Event> events = eventDao.findByEventFromAfterOrderByEventFromDesc(LocalDateTime.now());
		return eventInviteDao.findByInvitedAndEventIn(user, events);
	}

	public Integer getEventInviteCount(Long eventId) {
		return eventInviteDao.countByEvent(eventDao.getOne(eventId));
	}

	public Integer getEventInviteCount(Event event) {
		return eventInviteDao.countByEvent(eventDao.getOne(event.getEventId()));
	}

	public boolean userInvitedToEvent(User user, Event event) {
		if (eventInviteDao.countByInvitedAndEvent(user, event) > 0) {
			return true;
		} else {
			return false;
		}
	}

}
