package com.bh08.wee.service;

import java.util.List;
import java.util.Optional;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.wee.daos.DepartmentDAO;
import com.bh08.wee.daos.UserDAO;
import com.bh08.wee.exceptions.EmailInUseException;
import com.bh08.wee.exceptions.InvalidLoginException;
import com.bh08.wee.exceptions.NoUserException;
import com.bh08.wee.model.Department;
import com.bh08.wee.model.User;

@Service
public class UserService {

	@Autowired
	private UserDAO userDao;
	
	@Autowired
	private DepartmentDAO departmentDao;
	
	public List<User> getAllUsers() {
		return userDao.findAll();
	}
	
	public void updateUser(User user) {
		userDao.saveAndFlush(user);
	}
	
	public User saveUser(String userEmail, String password) {
		User user = new User();
		String encryptedPassword = DigestUtils.sha1Hex(password);
		user.setUserPassword(encryptedPassword);
		user.setUserEmail(userEmail);
		return userDao.saveAndFlush(user);
	}

	public User saveUser(User user) {
		String password = user.getUserPassword();
		String encryptedPassword = DigestUtils.sha1Hex(password);
		user.setUserPassword(encryptedPassword);
		return userDao.saveAndFlush(user);
	}
	
	
	public void saveNewUser(String userEmail, String password) throws EmailInUseException, NoUserException {
		
		System.out.println(getUserByUserEmail(userEmail));
		if (getUserByUserEmail(userEmail)==null) {
			User user = new User();
			String encryptedPassword = DigestUtils.sha1Hex(password);
			user.setUserPassword(encryptedPassword);
			user.setUserEmail(userEmail);
			userDao.saveAndFlush(user);
		}
	}

	public User getUserByUserEmail(String userEmail) throws NoUserException {

		Optional<User> validUser = userDao.findByUserEmail(userEmail);
		if(validUser.isPresent()) {
			User user = validUser.get();
			return user;
		}
		return null;
	}
	
	public User getUserByUserName(String userName) throws NoUserException {

		Optional<User> validUser = userDao.findByUserName(userName);
		if(validUser.isPresent()) {
			User user = validUser.get();
			return user;
		}
		return null;
	}

	public User getUserById(Long userId) throws NoUserException {
		Optional<User> user = userDao.findById(userId);
		if (user.isPresent()) {
			return user.get();
		} else {
			throw new NoUserException();
		}
	}

	public Optional<User> getOptionalUserById(Long userId)   {
		return userDao.findById(userId);
	}
	
	
	public List<User> getUsersByDepartment(Department department) {
		return userDao.findByUserDepartment(department);
	}

	public List<User> getUsersByName(String nameSearchExpression) {
		return userDao.findByUserNameContaining(nameSearchExpression);
	}

	public List<User> getUsersByCity(String city) {
		return userDao.findByUserCity(city);
	}

	public List<User> getUsersByCityLike(String citySearchExpression) {
		return userDao.findByUserCityContaining(citySearchExpression);
	}

	public List<User> getUsersByDepartmentAndName(Department department, String nameSearchExpression) {
		return userDao.findByUserDepartmentAndUserNameContaining(department, nameSearchExpression);
	}

	public List<User> getUsersByDepartmentId(Long departmentId) {
		return userDao.findByDepartmentId(departmentId);
	}



	public User checkLogin(String email, String password) throws InvalidLoginException, NoUserException {
		User user = getUserByUserEmail(email);
		String encryptedPassword = DigestUtils.sha1Hex(password);
		if (user.getUserPassword().equals(encryptedPassword)) {
			return user;
		} else {
			throw new InvalidLoginException();
		}

	}

}
