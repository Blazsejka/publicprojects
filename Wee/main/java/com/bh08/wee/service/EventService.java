package com.bh08.wee.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.wee.daos.EventDAO;
import com.bh08.wee.model.Event;
import com.bh08.wee.model.User;

@Service
public class EventService {

	@Autowired
	EventDAO eventDao;

	public Event saveEvent(Event event) {
		return eventDao.saveAndFlush(event);
	}

	public Optional<Event> getEventById(Long eventId) {
		return eventDao.findById(eventId);
	}

	public List<Event> getAllEvents() {
		return eventDao.findAll();
	}

	public List<Event> getFutureEvents() {
		return eventDao.findByEventFromAfterOrderByEventFromDesc(LocalDateTime.now());
	}

	public List<Event> getPastEvents() {
		return eventDao.findByEventFromBeforeOrderByEventFromDesc(LocalDateTime.now());
	}

	public List<Event> getEventsCreatedByUser(User user) {
		return eventDao.findByEventCreatorOrderByEventFromDesc(user);
	}

	public List<Event> getFutureEventsCreatedByUser(User user) {
		return eventDao.findByEventCreatorAndEventFromAfterOrderByEventFromDesc(user, LocalDateTime.now());
	}

	public List<Event> getPastEventsCreatedByUser(User user) {
		return eventDao.findByEventCreatorAndEventFromBeforeOrderByEventFromDesc(user, LocalDateTime.now());
	}

}
