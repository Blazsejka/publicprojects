package com.bh08.wee.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bh08.wee.builders.UserBuilder;
import com.bh08.wee.emailservice.EmailService;
import com.bh08.wee.exceptions.NoUserException;
import com.bh08.wee.model.Department;
import com.bh08.wee.model.Event;
import com.bh08.wee.model.EventInvite;
import com.bh08.wee.model.Gender;
import com.bh08.wee.model.User;
import com.bh08.wee.service.DepartmentService;
import com.bh08.wee.service.EventInviteService;
import com.bh08.wee.service.EventService;
import com.bh08.wee.service.UserService;

@Controller
public class InitController {

	@Autowired
	private EventService eventService;
	@Autowired
	private UserService userService;
	@Autowired
	private EventInviteService eventInviteService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	EmailService emailService;

	@RequestMapping(value = "test_usersearch", method = RequestMethod.GET)
	public String test_users(Model model) {
		// System.out.println(userService.getUsersByDepartment(departmentService.getDepartmentById(1L).get()));
		// System.out.println(userService.getUsersByName("John") );
		// System.out.println(userService.getUsersByDepartmentAndName(departmentService.getDepartmentById(1L).get(),
		// "John"));
		System.out.println(userService.getUsersByDepartmentId(1L));
		return "status";
	}

	@RequestMapping(value = "email", method = RequestMethod.GET)
	public String sendEmail() {

		emailService.sendSimpleMessage("bendaklara@gmail.com", "Hello World!", "It's a sunny day.");
		return "status.html";
	}	
	@RequestMapping(value = "test_events", method = RequestMethod.GET)
	public String displayEventTests(Model model) {
		System.out.println("Future events");
		List<Event> events = eventService.getFutureEvents();
		for (Event event : events) {
			System.out.println(event);
		}

		System.out.println("Past events");
		events = eventService.getPastEvents();
		for (Event event : events) {
			System.out.println(event);
		}

		System.out.println("User's events");
		try {
			events = eventService.getEventsCreatedByUser(userService.getUserById(1L));
			for (Event event : events) {
				System.out.println(event);
			}
		} catch (NoUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("User is invited");
		try {
			List<EventInvite> invites = eventInviteService.getInvitesForUser(userService.getUserById(2L));
			for (EventInvite invite : invites) {
				System.out.println(invite);
			}
		} catch (NoUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Invites for future events");
		List<EventInvite> invites = eventInviteService.getFutureInvites();
		for (EventInvite invite : invites) {
			System.out.println(invite);
		}

		System.out.println("Future invites for user");
		try {
			invites = eventInviteService.getFutureInvitesForUser(userService.getUserById(2L));
			for (EventInvite invite : invites) {
				System.out.println(invite);
			}
		} catch (NoUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Future invites for user");
		System.out.println(eventInviteService.getEventInviteCount(5L));

		return "status";
	}

	@RequestMapping(value = "init", method = RequestMethod.GET)

	public String init(Model model) {
		model.addAttribute("loggedInUserId", "0");

		Department d1 = departmentService.saveDepartment("Production");
		Department d2 = departmentService.saveDepartment("Research and Development");
		Department d3 = departmentService.saveDepartment("Purchasing");
		Department d4 = departmentService.saveDepartment("Marketing");
		Department d5 = departmentService.saveDepartment("Human Resource Management");
		Department d6 = departmentService.saveDepartment("Accounting and Finance");

		User user1 = new UserBuilder().setUserEmail("a@a.a").setUserPassword("a").setUserName("Ann Abbott")
				.setUserBirthDate(LocalDate.of(2000, 1, 18)).setUserGender(Gender.FEMALE).setUserBirthPlace("Ann Arbor")
				.setUserCity("Budapest").setUserDepartment(d1).build();

		User user2 = new UserBuilder().setUserEmail("b@b.b").setUserPassword("a").setUserName("Betty Brown")
				.setUserBirthDate(LocalDate.of(2001, 7, 30)).setUserGender(Gender.FEMALE)
				.setUserBirthPlace("Birmingham").setUserCity("Budapest").setUserDepartment(d1).build();

		User user3 = new UserBuilder().setUserEmail("c@c.c").setUserPassword("a").setUserName("Cindy Crawford")
				.setUserBirthDate(LocalDate.of(1970, 2, 13)).setUserGender(Gender.FEMALE).setUserBirthPlace("Chicago")
				.setUserCity("Budapest").setUserDepartment(d1).build();

		User user4 = new UserBuilder().setUserEmail("d@d.d").setUserPassword("a").setUserName("David Depp")
				.setUserBirthDate(LocalDate.of(1975, 4, 21)).setUserGender(Gender.MALE).setUserBirthPlace("Denver")
				.setUserCity("Budapest").setUserDepartment(d1).build();

		User user5 = new UserBuilder().setUserEmail("e@e.e").setUserPassword("a").setUserName("Emily Everest")
				.setUserBirthDate(LocalDate.of(1981, 5, 7)).setUserGender(Gender.FEMALE).setUserBirthPlace("Eaton")
				.setUserCity("Budapest").setUserDepartment(d1).build();

		User user6 = new UserBuilder().setUserEmail("f@f.f").setUserPassword("a").setUserName("Fanny Fargo")
				.setUserBirthDate(LocalDate.of(1987, 9, 8)).setUserGender(Gender.FEMALE).setUserBirthPlace("Fulton")
				.setUserCity("Budapest").setUserDepartment(d2).build();

		User user7 = new UserBuilder().setUserEmail("g@g.g").setUserPassword("a").setUserName("Gerry Ginsburg")
				.setUserBirthDate(LocalDate.of(1987, 9, 8)).setUserGender(Gender.MALE).setUserBirthPlace("Gainsborough")
				.setUserCity("Budapest").setUserDepartment(d2).build();

		User user8 = new UserBuilder().setUserEmail("h@h.h").setUserPassword("a").setUserName("Harry Hamilton")
				.setUserBirthDate(LocalDate.of(1961, 5, 21)).setUserGender(Gender.MALE).setUserBirthPlace("Huston")
				.setUserCity("Budapest").setUserDepartment(d2).build();

		User user9 = new UserBuilder().setUserEmail("i@i.i").setUserPassword("a").setUserName("Ian Innis")
				.setUserBirthDate(LocalDate.of(1997, 11, 21)).setUserGender(Gender.MALE)
				.setUserBirthPlace("Indianapolis").setUserCity("Budapest").setUserDepartment(d2).build();

		User user10 = new UserBuilder().setUserEmail("j@j.j").setUserPassword("a").setUserName("Joan John")
				.setUserBirthDate(LocalDate.of(1998, 11, 2)).setUserGender(Gender.FEMALE).setUserBirthPlace("Jakarta")
				.setUserCity("Budapest").setUserDepartment(d2).build();

		User user11 = new UserBuilder().setUserEmail("k@k.k").setUserPassword("a").setUserName("Kitty Kelly")
				.setUserBirthDate(LocalDate.of(1985, 6, 2)).setUserGender(Gender.FEMALE)
				.setUserBirthPlace("Kuala Lumpur").setUserCity("Budapest").setUserDepartment(d3).build();

		User user12 = new UserBuilder().setUserEmail("l@l.l").setUserPassword("a").setUserName("Laura Lopez")
				.setUserBirthDate(LocalDate.of(1985, 6, 2)).setUserGender(Gender.FEMALE)
				.setUserBirthPlace("Los Angeles").setUserCity("Budapest").setUserDepartment(d3).build();

		User user13 = new UserBuilder().setUserEmail("m@m.m").setUserPassword("a").setUserName("Madhav Murti")
				.setUserBirthDate(LocalDate.of(1985, 10, 1)).setUserGender(Gender.MALE).setUserBirthPlace("Mumbai")
				.setUserCity("Budapest").setUserDepartment(d4).build();

		User user14 = new UserBuilder().setUserEmail("n@n.n").setUserPassword("a").setUserName("Naomi Nite")
				.setUserBirthDate(LocalDate.of(1999, 2, 1)).setUserGender(Gender.FEMALE)
				.setUserBirthPlace("New Orleans").setUserCity("Budapest").setUserDepartment(d4).build();

		User user15 = new UserBuilder().setUserEmail("o@o.o").setUserPassword("a").setUserName("Oscar Osborn")
				.setUserBirthDate(LocalDate.of(1965, 9, 23)).setUserGender(Gender.MALE).setUserBirthPlace("Orlando")
				.setUserCity("Budapest").setUserDepartment(d5).build();

		User user16 = new UserBuilder().setUserEmail("ja@j.j").setUserPassword("a").setUserName("John Alistair")
				.setUserBirthDate(LocalDate.of(1990, 12, 23)).setUserGender(Gender.MALE).setUserBirthPlace("London")
				.setUserCity("Budapest").setUserDepartment(d5).build();

		User user17 = new UserBuilder().setUserEmail("jb@j.j").setUserPassword("a").setUserName("John McKinsey")
				.setUserBirthDate(LocalDate.of(1993, 9, 4)).setUserGender(Gender.MALE).setUserBirthPlace("London")
				.setUserCity("Budapest").setUserDepartment(d5).build();

		User user18 = new UserBuilder().setUserEmail("jc@j.j").setUserPassword("a").setUserName("John Smith")
				.setUserBirthDate(LocalDate.of(2001, 8, 4)).setUserGender(Gender.MALE).setUserBirthPlace("London")
				.setUserCity("Budapest").setUserDepartment(d5).build();

		User user19 = new UserBuilder().setUserEmail("jd@j.j").setUserPassword("a").setUserName("John Giddens")
				.setUserBirthDate(LocalDate.of(2003, 8, 18)).setUserGender(Gender.MALE).setUserBirthPlace("London")
				.setUserCity("Budapest").setUserDepartment(d5).build();

		User user20 = new UserBuilder().setUserEmail("jf@j.j").setUserPassword("a").setUserName("John Woodhouse")
				.setUserBirthDate(LocalDate.of(2002, 5, 18)).setUserGender(Gender.MALE).setUserBirthPlace("London")
				.setUserCity("Budapest").setUserDepartment(d1).build();

		user1 = userService.saveUser(user1);
		user2 = userService.saveUser(user2);
		user3 = userService.saveUser(user3);
		user4 = userService.saveUser(user4);
		user5 = userService.saveUser(user5);
		user6 = userService.saveUser(user6);
		user7 = userService.saveUser(user7);
		user8 = userService.saveUser(user8);
		user9 = userService.saveUser(user9);
		user10 = userService.saveUser(user10);
		user11 = userService.saveUser(user11);
		user12 = userService.saveUser(user12);
		user13 = userService.saveUser(user13);
		user14 = userService.saveUser(user14);
		user15 = userService.saveUser(user15);
		user16 = userService.saveUser(user16);
		user17 = userService.saveUser(user17);
		user18 = userService.saveUser(user18);
		user19 = userService.saveUser(user19);
		user20 = userService.saveUser(user20);

		Event event = new Event();
		event.setEventCreator(user1);
		event.setEventTitle("Event 1");
		event.setEventFrom(LocalDateTime.now());
		event.setEventUntil(LocalDateTime.now());
		event.setEventLocation("Budapest");
		eventService.saveEvent(event);
		Event event2 = new Event();
		event2.setEventCreator(user1);
		event2.setEventTitle("Event 2");
		event2.setEventFrom(LocalDateTime.now());
		event2.setEventUntil(LocalDateTime.now());
		event2.setEventLocation("London");
		eventService.saveEvent(event2);
		Event event3 = new Event();
		event3.setEventCreator(user1);
		event3.setEventTitle("Event 3");
		event3.setEventFrom(LocalDateTime.now());
		event3.setEventUntil(LocalDateTime.now());
		event3.setEventLocation("Parizs");
		eventService.saveEvent(event3);
		EventInvite eventInvite = new EventInvite();
		eventInvite.setEvent(event);
		eventInvite.setInviteCreated(new Date());
		eventInvite.setInviteValid(true);
		eventInvite.setInvited(user1);
		EventInvite eventInvite2 = new EventInvite();
		eventInvite2.setEvent(event);
		eventInvite2.setInviteCreated(new Date());
		eventInvite2.setInviteValid(true);
		eventInvite2.setInvited(user2);
		EventInvite eventInvite3 = new EventInvite();
		eventInvite3.setEvent(event2);
		eventInvite3.setInviteCreated(new Date());
		eventInvite3.setInviteValid(true);
		eventInvite3.setInvited(user2);
		EventInvite eventInvite4 = new EventInvite();
		eventInvite4.setEvent(event3);
		eventInvite4.setInviteCreated(new Date());
		eventInvite4.setInviteValid(true);
		eventInvite4.setInvited(user2);
		eventInviteService.saveEventInvite(eventInvite);
		eventInviteService.saveEventInvite(eventInvite2);
		eventInviteService.saveEventInvite(eventInvite3);
		eventInviteService.saveEventInvite(eventInvite4);

		// System.out.println(eventService.getAllEvents());
		System.out.println(eventInviteService.getAllEventInvites());
		return "init.html";
	}

}
