package com.bh08.wee.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bh08.wee.viewmodel.data.PageData;

@Controller
public class ErrorPageController {
	
	@RequestMapping(value = "data_access_error", method = RequestMethod.GET)
	public String displayErrorPage(Model model) {
		PageData pageData = new PageData();
		pageData.setMenuLocation("fragments/menus :: errorMenu");
		pageData.setPageLocation("fragments/error :: errorPage");
		pageData.setMainClass("error");
		model.addAttribute("pageData", pageData);
		return "main.html";
	}

}
