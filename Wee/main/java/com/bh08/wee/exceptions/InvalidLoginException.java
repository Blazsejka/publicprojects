package com.bh08.wee.exceptions;

public class InvalidLoginException extends Exception {

	public InvalidLoginException() {
		super("Wrong email or password!");
	}
}
