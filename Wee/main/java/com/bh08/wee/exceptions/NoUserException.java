package com.bh08.wee.exceptions;

public class NoUserException extends Exception {

	public NoUserException() {
		super("There is no registrated user with this email address.");
	}
	
}
