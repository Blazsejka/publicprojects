package com.bh08.wee.exceptions;

public class EmailInUseException extends Exception {

	public EmailInUseException() {
		super("Email address is already in use.");
	}
	
}
