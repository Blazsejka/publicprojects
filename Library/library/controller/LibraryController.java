package library.controller;

import java.sql.Statement;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import library.model.Book;
import library.model.User;
import library.view.LibraryView;

public class LibraryController {

    LibraryView libraryView;
    Book book;
    Writer writer;
    User user;

    List<Book> shelf = new ArrayList<>();
    List<User> box = new ArrayList<>();

    public LibraryController() {
        this.libraryView = new LibraryView(this);
    }

    public void handleListAllButtonClick() throws SQLException {
        libraryView.setListAllWindowVisible();
        libraryView.listAllArea.setText(" ");

        try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "12345c")) {

            String selectSQL = "SELECT * FROM BOOKS";
            PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Book tmpbook = new Book();
                tmpbook.setISBN(rs.getString("ISBN"));
                tmpbook.setTitle(rs.getString("title"));
                tmpbook.setWriter(rs.getString("writer"));
                if (rs.getString("availability") == "yes") {
                    tmpbook.setAvailability(true);
                } else {
                    tmpbook.setAvailability(false);
                }
                shelf.add(tmpbook);
            }
            
            libraryView.listAllArea.setText(shelf.toString());

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void handleListAvailableButtonClick() {
        libraryView.setListAvailableWindowVisible();
        libraryView.listAvailableArea.setText(" ");
        List<Book> availables = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "12345c")) {

            String selectSQL = "SELECT * FROM BOOKS WHERE AVAILABILITY = 'yes' ";
            PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Book tmpbook = new Book();
                tmpbook.setISBN(rs.getString("ISBN"));
                tmpbook.setTitle(rs.getString("title"));
                tmpbook.setWriter(rs.getString("writer"));
                tmpbook.setAvailability(true);
                availables.add(tmpbook);
            }

            libraryView.listAvailableArea.setText(availables.toString());

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void handleListNotAvailableButtonClick() {
        libraryView.setListNotAvailableWindowVisible();
        libraryView.listNotAvailableArea.setText(" ");
        List<Book> notavailables = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "12345c")) {

            String selectSQL = "SELECT * FROM BOOKS WHERE AVAILABILITY = 'no'";

            PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Book tmpbook = new Book();
                tmpbook.setISBN(rs.getString("ISBN"));
                tmpbook.setTitle(rs.getString("title"));
                tmpbook.setWriter(rs.getString("writer"));
                tmpbook.setAvailability(false);
                notavailables.add(tmpbook);

            }

            libraryView.listNotAvailableArea.setText(notavailables.toString());

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void handleFindButtonClick() {
        libraryView.setFindWindowVisible();
    }

    public void handleBorrowButtonClick() {
        libraryView.setBorrowWindowVisible();
    }

    public void handleRegistrationButtonClick() {
        libraryView.setRegistrationWindowVisible();
    }

    public void handleRegNewUserButtonClick() {
        User tmp = new User();
        String tmpname = null;
        String tmpaddress = null;

        if (libraryView.userNameField.getText().toString().equals("")) {
            libraryView.showRegistrationError("The name is empty. Please fill it!");
        } else {
            tmpname = libraryView.userNameField.getText().toString();
        }

        if (libraryView.userAddressField.getText().toString().equals("")) {
            libraryView.showRegistrationError("The address is empty. Please fill it!");
        } else {
            tmpaddress = libraryView.userAddressField.getText().toString();
        }

        tmp.setName(tmpname);
        tmp.setAddress(tmpaddress);
        box.add(tmp);

        try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "12345c")) {
            String selectSQL = "insert into users (user_name, user_address)\n" + "values ('" + tmpname + "', '" + tmpaddress + "')";
            PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        libraryView.userAddressField.setText("");
        libraryView.userAddressField.setText("");
        libraryView.registrationFrame.setVisible(false);
    }

    public void handleBookBorrowButtonClick() {
        Book borrowBook = new Book();
        User borrowUser = new User();
        String borrowtitle = null;
        String borrowisbn = null;
        String borrowname = null;

        if (libraryView.bookTitleField.getText().toString().equals("")) {
            libraryView.showBorrowError("The title is empty. Please fill it!");
        } else {
            borrowtitle = libraryView.bookTitleField.getText().toString();
        }

        if (libraryView.bookISBNField.getText().toString().equals("")) {
            libraryView.showBorrowError("The ISBN is empty. Please fill it!");
        } else {
            borrowisbn = libraryView.bookISBNField.getText().toString();
        }

        if (libraryView.borrowUserNameField.getText().toString().equals("")) {
            libraryView.showBorrowError("The username is empty. Please fill it!");
        } else {
            borrowname = libraryView.borrowUserNameField.getText().toString();
        }

        try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "12345c")) {

            String selectSQL = "SELECT * FROM BOOKS WHERE lower(ISBN) like ? and lower(TITLE) like ?";
            PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
            preparedStatement.setString(1, "%" + borrowisbn.toLowerCase() + "%");
            preparedStatement.setString(2, "%" + borrowtitle.toLowerCase() + "%");
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                borrowBook.setISBN(rs.getString("ISBN"));
                borrowBook.setTitle(rs.getString("title"));
                borrowBook.setWriter(rs.getString("writer"));
                if (rs.getString("availability").equals("yes")) {
                    borrowBook.setAvailability(true);
                } else {
                    borrowBook.setAvailability(false);
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "12345c")) {

            String selectSQL = "SELECT * FROM USERS WHERE lower (user_name) like ? ";
            PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
            preparedStatement.setString(1, '%' + borrowname.toLowerCase() + '%');
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                borrowUser.setName(rs.getString("user_name"));
                borrowUser.setAddress(rs.getString("user_address"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        if (borrowBook.isAvailability()) {
            libraryView.showBorrowError(borrowUser + " have borrowed the " + borrowBook + " for 2 weeks!");
            try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "12345c")) {
                String selectSQL = "update books set lower (availability) = 'no' where lower (isbn) = '?'";
                PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
                preparedStatement.setString(1, '%' + borrowisbn.toLowerCase() + '%');
                preparedStatement.executeUpdate();

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            
        } else {
            libraryView.showBorrowError(borrowUser + " cannot borrow the " + borrowBook + " because it is not available right now!");
        }

        libraryView.bookTitleField.setText("");
        libraryView.bookISBNField.setText("");
        libraryView.borrowUserNameField.setText("");
    }

    public void handleFindBookButtonClick() {
        Book findbook = new Book();
        String findtitle = null;
        String findwriter = null;

        if (libraryView.findBookTitleField.getText().toString().equals("")) {
            libraryView.showFindError("The title is empty. Please fill it!");
        } else {
            findtitle = libraryView.findBookTitleField.getText().toString();
        }

        if (libraryView.findBookWriterField.getText().toString().equals("")) {
            libraryView.showFindError("The writer is empty. Please fill it!");
        } else {
            findwriter = libraryView.findBookWriterField.getText().toString();
        }

        try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "12345c")) {

            String selectSQL = "SELECT * FROM BOOKS WHERE lower (title) like ? and lower (writer) like ? ";

            PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
            preparedStatement.setString(1, '%' + findtitle.toLowerCase() + '%');
            preparedStatement.setString(2, '%' + findwriter.toLowerCase() + '%');
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                findbook.setISBN(rs.getString("ISBN"));
                findbook.setTitle(rs.getString("title"));
                findbook.setWriter(rs.getString("writer"));
                if (rs.getString("availability").equals("yes")) {
                    findbook.setAvailability(true);
                } else {
                    findbook.setAvailability(false);
                }
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        libraryView.bookFoundField.setText(findbook.toString());

        libraryView.findBookTitleField.setText("");
        libraryView.findBookWriterField.setText("");

    }

}
