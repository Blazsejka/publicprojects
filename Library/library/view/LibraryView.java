
package library.view;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import library.controller.LibraryController;

public class LibraryView {
    
    public JFrame mainFrame = new JFrame();
    public JFrame listAllFrame = new JFrame();
    public JFrame listAvailableFrame = new JFrame();
    public JFrame listNotAvailableFrame = new JFrame();
    public JFrame findFrame = new JFrame();
    public JFrame borrowFrame = new JFrame();
    public JFrame registrationFrame = new JFrame();
    protected LibraryController LibraryController;
    Font f = new Font("Courier New", Font.PLAIN, 12);
    private JPanel mainPanel;
    private JPanel listAllPanel;
    private JPanel listAvailablePanel;
    private JPanel listNotAvailablePanel;
    private JButton listAllButton;
    public JTextArea listAllArea;
    private JButton listAvailableButton;
    private JButton listNotAvailableButton;
    public JTextArea listAvailableArea;
    public JTextArea listNotAvailableArea;
    private JPanel findPanel;
    private JButton findButton;
    private JPanel borrowPanel;
    private JButton borrowButton;
    private JButton registrationButton;
    private JPanel registrationPanel;
    private JLabel userNameLabel;
    public JTextField userNameField;
    private JLabel userAddressLabel;
    public JTextField userAddressField;
    private JButton regNewUserButton;
    private JLabel bookTitleLabel;
    public JTextField bookTitleField;
    private JLabel bookISBNLabel;
    public JTextField bookISBNField;
    private JButton bookBorrowButton;
    private JLabel findBookTitleLabel;
    public JTextField findBookTitleField;
    private JLabel findBookWriterLabel;
    public JTextField findBookWriterField;
    private JButton findBookButton;
    public JTextField bookFoundField;
    private JLabel borrowUserNameLabel;
    public JTextField borrowUserNameField;
            
            
            
            
    public LibraryView(LibraryController libraryController) {
        this.LibraryController=libraryController;
        init();
    }
 
    
    private void init() {    
        setupMainWindow();
        setupListAllButton();
        setupListAllWindow();
        setupListAllArea();
        setupListAvailableButton();
        setupListAvailableWindow();
        setupListAvailableArea();
        setupListNotAvailableWindow();
        setupListNotAvailableButton();
        setupListNotAvailableArea();
        setupFindWindow();
        setupFindButton();
        setupBorrowWindow();
        setupBorrowButton();
        setupRegistrationWindow();
        setupRegistrationButton();
        setupUserNameField();
        setupUserNameLabel();
        setupUserAddressLabel();
        setupUserAddressField();
        setupRegNewUserButton();
        setupBookISBNField();
        setupBookISBNLabel();
        setupBookTitleField();
        setupBookTitleLabel();
        setupBookBorrowButton();
        setupBookFoundField();
        setupFindBookButton();
        setupFindBookTitleField();
        setupFindBookTitleLabel();
        setupFindBookWriterField();
        setupFindBookWriterLabel();
        setupBorrowUserNameField();
        setupBorrowUserNameLabel();
        
        mainFrame.setVisible(true);
        listAllFrame.setVisible(false);
        listAvailableFrame.setVisible(false);
        listNotAvailableFrame.setVisible(false);
        findFrame.setVisible(false);
        
    }
  
    
    
    
    private void setupMainWindow() {
        mainFrame.setSize(900,100);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setTitle("Library stock program");
        mainFrame.setLayout(new GridLayout(1, 1));
        mainPanel = new JPanel();
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        mainFrame.add(mainPanel);
        GridBagLayout gbl = new GridBagLayout();
        mainPanel.setLayout(gbl);
        mainFrame.setResizable(false);
    }
    
    
    private void setupListAllWindow() {
        listAllFrame.setSize(500,500);
        listAllFrame.setLocationRelativeTo(null);
        listAllFrame.setTitle("All books");
        listAllFrame.setLayout(new GridLayout(1, 1));
        listAllPanel = new JPanel();
        listAllPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        listAllFrame.add(listAllPanel);
        GridBagLayout gbl = new GridBagLayout();
        listAllPanel.setLayout(gbl);
        listAllFrame.setResizable(false);
        
    }
    
        
    public void setListAllWindowVisible() {
        listAllFrame.setVisible(true);
    }
    
    private void setupListAllArea() {
        this.listAllArea = new JTextArea(" ");
        this.listAllArea.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        listAllArea.setEditable(false);
        gbc.fill = GridBagConstraints.BOTH;
        listAllPanel.add(listAllArea, gbc);
        listAllArea.setLineWrap(true);
    }
    
    
    private void setupListAvailableWindow() {
        listAvailableFrame.setSize(500,500);
        listAvailableFrame.setLocationRelativeTo(null);
        listAvailableFrame.setTitle("Available books");
        listAvailableFrame.setLayout(new GridLayout(1, 1));
        listAvailablePanel = new JPanel();
        listAvailablePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        listAvailableFrame.add(listAvailablePanel);
        GridBagLayout gbl = new GridBagLayout();
        listAvailablePanel.setLayout(gbl);
        listAvailableFrame.setResizable(false);
    }
    
    public void setListAvailableWindowVisible() {
        listAvailableFrame.setVisible(true);
    }
    
    private void setupListAvailableArea() {
        this.listAvailableArea = new JTextArea("");
        this.listAvailableArea.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        listAvailableArea.setEditable(false);
        gbc.fill = GridBagConstraints.BOTH;
        listAvailablePanel.add(listAvailableArea, gbc);
        listAvailableArea.setLineWrap(true);
    }
    
    
    private void setupListNotAvailableWindow() {
        listNotAvailableFrame.setSize(500,500);
        listNotAvailableFrame.setLocationRelativeTo(null);
        listNotAvailableFrame.setTitle("Not available books");
        listNotAvailableFrame.setLayout(new GridLayout(1, 1));
        listNotAvailablePanel = new JPanel();
        listNotAvailablePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        listNotAvailableFrame.add(listNotAvailablePanel);
        GridBagLayout gbl = new GridBagLayout();
        listNotAvailablePanel.setLayout(gbl);
        listNotAvailableFrame.setResizable(false);
    }
    
    public void setListNotAvailableWindowVisible() {
        listNotAvailableFrame.setVisible(true);
    }
    
    private void setupListNotAvailableArea() {
        this.listNotAvailableArea = new JTextArea("");
        this.listNotAvailableArea.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        listNotAvailableArea.setEditable(false);
        gbc.fill = GridBagConstraints.BOTH;
        listNotAvailablePanel.add(listNotAvailableArea, gbc);
        listNotAvailableArea.setLineWrap(true);
    }
    
    private void setupFindWindow() {
        findFrame.setSize(500,350);
        findFrame.setLocationRelativeTo(null);
        findFrame.setTitle("Find books");
        findFrame.setLayout(new GridLayout(1, 1));
        findPanel = new JPanel();
        findPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        findFrame.add(findPanel);
        GridBagLayout gbl = new GridBagLayout();
        findPanel.setLayout(gbl);
        findFrame.setResizable(false);
        
    }
    
    public void setFindWindowVisible() {
        findFrame.setVisible(true);
    }
    
    private void setupFindButton() {
        GridBagConstraints gbc;
        this.findButton = new JButton("Find books");
        this.findButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(findButton, gbc);
        
        ActionListener findAction = f -> {    
            LibraryController.handleFindButtonClick();
        };
        findButton.addActionListener(findAction);
    }
    
    private void setupBorrowWindow() {
        borrowFrame.setSize(500,250);
        borrowFrame.setLocationRelativeTo(null);
        borrowFrame.setTitle("Borrow books");
        borrowFrame.setLayout(new GridLayout(1, 1));
        borrowPanel = new JPanel();
        borrowPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        borrowFrame.add(borrowPanel);
        GridBagLayout gbl = new GridBagLayout();
        borrowPanel.setLayout(gbl);
        borrowFrame.setResizable(false);
        
    }
    
    public void setBorrowWindowVisible() {
        borrowFrame.setVisible(true);
    }
    
    private void setupBorrowButton() {
        GridBagConstraints gbc;
        this.borrowButton = new JButton("Borrow books");
        this.borrowButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 4;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(borrowButton, gbc);
        
        ActionListener borrowAction = b -> {    
            LibraryController.handleBorrowButtonClick();
        };
        borrowButton.addActionListener(borrowAction);
    }
    
    
    private void setupRegistrationWindow() {
        registrationFrame.setSize(500,250);
        registrationFrame.setLocationRelativeTo(null);
        registrationFrame.setTitle("New client");
        registrationFrame.setLayout(new GridLayout(1, 1));
        registrationPanel = new JPanel();
        registrationPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        registrationFrame.add(registrationPanel);
        GridBagLayout gbl = new GridBagLayout();
        registrationPanel.setLayout(gbl);
        registrationFrame.setResizable(false);
        
    }
    
    public void setRegistrationWindowVisible() {
        registrationFrame.setVisible(true);
    }
    
    private void setupRegistrationButton() {
        GridBagConstraints gbc;
        this.registrationButton = new JButton("New user");
        this.registrationButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(registrationButton, gbc);
        
        ActionListener registrationAction = r -> {    
            LibraryController.handleRegistrationButtonClick();
        };
        registrationButton.addActionListener(registrationAction);
    }
    
    
    private void setupUserNameLabel() {
        GridBagConstraints gbc;
        this.userNameLabel = new JLabel("Name:");
        this.userNameLabel.setFont(f);
        userNameLabel.setHorizontalAlignment(userNameLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        registrationPanel.add(userNameLabel, gbc);                
    }
    
    private void setupUserNameField() {
        this.userNameField = new JTextField("");
        this.userNameField.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        registrationPanel.add(userNameField, gbc);
    }
    
    private void setupUserAddressLabel() {
        GridBagConstraints gbc;
        this.userAddressLabel = new JLabel("Address:");
        this.userAddressLabel.setFont(f);
        userAddressLabel.setHorizontalAlignment(userAddressLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        registrationPanel.add(userAddressLabel, gbc);                
    }
    
    private void setupUserAddressField() {
        this.userAddressField = new JTextField("");
        this.userAddressField.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        registrationPanel.add(userAddressField, gbc);
    }
    
    private void setupRegNewUserButton() {
        GridBagConstraints gbc;
        this.regNewUserButton = new JButton("Registrate");
        this.regNewUserButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        registrationPanel.add(regNewUserButton, gbc);
        
        ActionListener regNewUserAction = r -> {    
            LibraryController.handleRegNewUserButtonClick();
        };
        regNewUserButton.addActionListener(regNewUserAction);
    }
    
    
    private void setupBookTitleLabel() {
        GridBagConstraints gbc;
        this.bookTitleLabel = new JLabel("Title:");
        this.bookTitleLabel.setFont(f);
        bookTitleLabel.setHorizontalAlignment(bookTitleLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        borrowPanel.add(bookTitleLabel, gbc);                
    }
    
    private void setupBookTitleField() {
        this.bookTitleField = new JTextField("");
        this.bookTitleField.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        borrowPanel.add(bookTitleField, gbc);
    }
    
    private void setupBookISBNLabel() {
        GridBagConstraints gbc;
        this.bookISBNLabel = new JLabel("ISBN:");
        this.bookISBNLabel.setFont(f);
        bookISBNLabel.setHorizontalAlignment(bookISBNLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        borrowPanel.add(bookISBNLabel, gbc);                
    }
    
    private void setupBookISBNField() {
        this.bookISBNField = new JTextField("");
        this.bookISBNField.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        borrowPanel.add(bookISBNField, gbc);
    }
    
    private void setupBorrowUserNameLabel() {
        GridBagConstraints gbc;
        this.borrowUserNameLabel = new JLabel("User name:");
        this.borrowUserNameLabel.setFont(f);
        borrowUserNameLabel.setHorizontalAlignment(borrowUserNameLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        borrowPanel.add(borrowUserNameLabel, gbc);                
    }
    
    private void setupBorrowUserNameField() {
        this.borrowUserNameField = new JTextField("");
        this.borrowUserNameField.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        borrowPanel.add(borrowUserNameField, gbc);
    }
    
    private void setupBookBorrowButton() {
        GridBagConstraints gbc;
        this.bookBorrowButton = new JButton("Borrow");
        this.bookBorrowButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        borrowPanel.add(bookBorrowButton, gbc);
        
        ActionListener bookBorrowAction = b -> {    
            LibraryController.handleBookBorrowButtonClick();
        };
        bookBorrowButton.addActionListener(bookBorrowAction);
    }

    
    private void setupFindBookTitleLabel() {
        GridBagConstraints gbc;
        this.findBookTitleLabel = new JLabel("Title:");
        this.findBookTitleLabel.setFont(f);
        findBookTitleLabel.setHorizontalAlignment(findBookTitleLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        findPanel.add(findBookTitleLabel, gbc);                
    }
    
    private void setupFindBookTitleField() {
        this.findBookTitleField = new JTextField("");
        this.findBookTitleField.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        findPanel.add(findBookTitleField, gbc);
    }
    
    private void setupFindBookWriterLabel() {
        GridBagConstraints gbc;
        this.findBookWriterLabel = new JLabel("Writer:");
        this.findBookWriterLabel.setFont(f);
        findBookWriterLabel.setHorizontalAlignment(findBookWriterLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        findPanel.add(findBookWriterLabel, gbc);                
    }
    
    private void setupFindBookWriterField() {
        this.findBookWriterField = new JTextField("");
        this.findBookWriterField.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        findPanel.add(findBookWriterField, gbc);
    }
    
    private void setupFindBookButton() {
        GridBagConstraints gbc;
        this.findBookButton = new JButton("Find book");
        this.findBookButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        findPanel.add(findBookButton, gbc);
        
        ActionListener findBookAction = f -> {    
            LibraryController.handleFindBookButtonClick();
        };
        findBookButton.addActionListener(findBookAction);
    }

    private void setupBookFoundField() {
        this.bookFoundField = new JTextField("");
        this.bookFoundField.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        findPanel.add(bookFoundField, gbc);
        bookFoundField.setEditable(false);
    }
    
    
    private void setupListAllButton() {
        GridBagConstraints gbc;
        this.listAllButton = new JButton("All books");
        this.listAllButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(listAllButton, gbc);
        
        ActionListener listAllAction = l -> {    
            try {
                LibraryController.handleListAllButtonClick();
            } catch (SQLException ex) {
                Logger.getLogger(LibraryView.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        listAllButton.addActionListener(listAllAction);
    }
    
    
    private void setupListAvailableButton() {
        GridBagConstraints gbc;
        this.listAvailableButton = new JButton("Available books");
        this.listAvailableButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(listAvailableButton, gbc);
        
        ActionListener listAvailableAction = l -> {    
            LibraryController.handleListAvailableButtonClick();
        };
        listAvailableButton.addActionListener(listAvailableAction);
    }
    
    
    private void setupListNotAvailableButton() {
        GridBagConstraints gbc;
        this.listNotAvailableButton = new JButton("Not available books");
        this.listNotAvailableButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(listNotAvailableButton, gbc);
        
        ActionListener listNotAvailableAction = l -> {    
            LibraryController.handleListNotAvailableButtonClick();
        };
        listNotAvailableButton.addActionListener(listNotAvailableAction);
    }
    
    
    public void showRegistrationError(String text) {
        JOptionPane.showMessageDialog(registrationFrame, text);
    }
    
    public void showFindError(String text) {
        JOptionPane.showMessageDialog(findFrame, text);
    }
    
    public void showBorrowError(String text) {
        JOptionPane.showMessageDialog(borrowFrame, text);
    }
    
}
