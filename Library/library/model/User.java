
package library.model;

public class User {
    
    
    private int userId;
    private String userName;
    private String userAddress;

    public User() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return userName;
    }

    public void setName(String name) {
        this.userName = name;
    }

    public String getAddress() {
        return userAddress;
    }

    public void setAddress(String address) {
        this.userAddress = address;
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", name=" + userName + ", address=" + userAddress + '}';
    }
    
    
    
    
    
}
