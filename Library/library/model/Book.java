
package library.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Book {
    
    private String title;
    private String ISBN;
    private String writer;
    private boolean availability;

    public Book() {
    }

    public Book(String title, String ISBN, String writer, boolean availability) {
        this.title = title;
        this.ISBN = ISBN;
        this.writer = writer;
        this.availability = availability;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.title);
        hash = 17 * hash + Objects.hashCode(this.ISBN);
        hash = 17 * hash + Objects.hashCode(this.writer);
        hash = 17 * hash + (this.availability ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Book other = (Book) obj;
        if (this.availability != other.availability) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.ISBN, other.ISBN)) {
            return false;
        }
        if (!Objects.equals(this.writer, other.writer)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Book{" + "title=" + title + ", ISBN=" + ISBN + ", writer=" + writer + ", availability=" + availability + '}';
    }

    
    
    
}
