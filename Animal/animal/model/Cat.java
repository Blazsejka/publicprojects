
package animal.model;

public class Cat extends Animal {
    
    private boolean isMeowing;
    private boolean isPurring;

    public Cat() {
    }
    
       
    public Cat(boolean isMeowing, boolean isPurring, String name, String color, int price) {
        super(name, color, price);
        this.isMeowing = isMeowing;
        this.isPurring = isPurring;
    }

    public boolean isIsMeowing() {
        return isMeowing;
    }

    public void setIsMeowing(boolean isMeowing) {
        this.isMeowing = isMeowing;
    }

    public boolean isIsPurring() {
        return isPurring;
    }

    public void setIsPurring(boolean isPurring) {
        this.isPurring = isPurring;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.isMeowing ? 1 : 0);
        hash = 19 * hash + (this.isPurring ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cat other = (Cat) obj;
        if (this.isMeowing != other.isMeowing) {
            return false;
        }
        if (this.isPurring != other.isPurring) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cat{" + "name=" + name + ", color=" + color + ", price=" + price + "isMeowing=" + isMeowing + ", isPurring=" + isPurring + '}';
    }
    
    
    
}
