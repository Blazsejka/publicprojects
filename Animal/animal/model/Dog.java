
package animal.model;

public class Dog extends Animal {
    
    private boolean isBarking;
    private boolean isBiting;

    public Dog() {
    }
    
    public Dog(boolean isBarking, boolean isBiting, String name, String color, int price) {
        super(name, color, price);
        this.isBarking = isBarking;
        this.isBiting = isBiting;
    }

    public boolean isIsBarking() {
        return isBarking;
    }

    public void setIsBarking(boolean isBarking) {
        this.isBarking = isBarking;
    }

    public boolean isIsBiting() {
        return isBiting;
    }

    public void setIsBiting(boolean isBiting) {
        this.isBiting = isBiting;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.isBarking ? 1 : 0);
        hash = 97 * hash + (this.isBiting ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dog other = (Dog) obj;
        if (this.isBarking != other.isBarking) {
            return false;
        }
        if (this.isBiting != other.isBiting) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dog{" + "name=" + name + ", color=" + color + ", price=" + price + "isBarking=" + isBarking + ", isBiting=" + isBiting + '}';
    }
    
    
    
    
}
