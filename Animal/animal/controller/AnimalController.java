package animal.controller;

import animal.model.Animal;
import animal.model.Cat;
import animal.model.Dog;
import animal.view.AnimalView;
import java.util.ArrayList;
import java.util.List;

public class AnimalController {

    AnimalView animalView;
    private static AnimalController av = null;
    private List<Animal> stable = new ArrayList<>();
    List<Animal> dogs = new ArrayList<>();
    List<Animal> cats = new ArrayList<>();

    public AnimalController() {
        baseUpload();
        this.animalView = new AnimalView(this);
    }

    private void baseUpload() {
        stable.add(new Dog(true, true, "Morzsi", "black", 5000));
        stable.add(new Dog(true, false, "Buksi", "white", 10000));
        stable.add(new Cat(true, false, "Cirmi", "pinto", 2000));
        stable.add(new Cat(false, true, "Gombi", "mocha", 3000));
    }

    public void handleBuyButtonClick() {
        if (animalView.dogcatBox.getSelectedItem().equals("dog")) {
            Dog tmpDog = new Dog();
                tmpDog.setColor(animalView.colorField.getText());
                tmpDog.setName(animalView.nameField.getText());
                tmpDog.setPrice(Integer.parseInt(animalView.priceField.getText()));
            if (animalView.soundBox.getSelectedItem().equals("yes")) {
                tmpDog.setIsBarking(true);
            } else {
                tmpDog.setIsBarking(false);
            }

            if (animalView.biteBox.getSelectedItem().equals("yes")) {
                tmpDog.setIsBiting(true);
            } else {
                tmpDog.setIsBiting(false);
            }
            stable.add(tmpDog);
        } else {
            Cat tmpCat = new Cat();
            tmpCat.setColor(animalView.colorField.getText());
            tmpCat.setName(animalView.nameField.getText());
            tmpCat.setPrice(Integer.parseInt(animalView.priceField.getText()));

            if (animalView.soundBox.getSelectedItem().equals("yes")) {
                tmpCat.setIsMeowing(true);
            } else {
                tmpCat.setIsMeowing(false);
            }

            if (animalView.biteBox.getSelectedItem().equals("yes")) {
                tmpCat.setIsPurring(true);
            } else {
                tmpCat.setIsPurring(false);
            }
            stable.add(tmpCat);
        }

    }


    public void handleListButtonClick() {
//        
//        for (Animal s : stable) {
//            if (s.getClass().equals(Dog)) {
//                dogs.add(s);
//            }
//            else cats.add(s);
//        }
//        
//        animalView.dogsArea.setText(dogs.toString());
//        animalView.catsArea.setText(cats.toString());

        animalView.dogsArea.setText(stable.toString());

    }

}
