
package animal.view;

import animal.controller.AnimalController;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AnimalView {
    
    
    private AnimalController animalController;
    private JFrame frame = new JFrame();
    private Font f = new Font("Courier New", Font.BOLD, 20);
    private JPanel panel;
    public JTextArea dogsArea;
    public JTextArea catsArea;
    private JButton buyButton;
    private JLabel nameLabel;
    public JTextField nameField;
    private JLabel colorLabel;
    public JTextField colorField;
    private JLabel priceLabel;
    public JTextField priceField;
    private JLabel soundLabel;
    public JComboBox soundBox;
    private JLabel biteLabel;
    public JComboBox biteBox;
    public JComboBox dogcatBox;
    private JButton listButton;
    

    
    public AnimalView(AnimalController animalController) {
        this.animalController=animalController;
        init();
    }
    
    
    private void init() {    
        setupWindow();
        setupBiteBox();
        setupBiteLabel();
        setupBuyButton();
        setupColorField();
        setupColorLabel();
        setupNameField();
        setupNameLabel();
        setupPriceField();
        setupPriceLabel();
        setupSoundBox();
        setupSoundLabel();
        setupdogCatBox();
        setupCatsArea();
        setupDogsArea();
        setupListButton();
        frame.setVisible(true);
    }
  
       
    private void setupWindow() {
        frame.setSize(800,900);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Animals");
        frame.setLayout(new GridLayout(1, 1));
        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        frame.add(panel);
        GridBagLayout gbl = new GridBagLayout();
        panel.setLayout(gbl);
        frame.setResizable(false);
    } 
    
    private void setupDogsArea() {
        this.dogsArea = new JTextArea();
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        dogsArea.setLineWrap(true);
        dogsArea.setEditable(false);
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(dogsArea, gbc);
    }
    
    private void setupCatsArea() {
        this.catsArea = new JTextArea();
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        catsArea.setLineWrap(true);
        catsArea.setEditable(false);
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(catsArea, gbc);
    }
    
    
    private void setupdogCatBox() {
        String[] animal = new String[] {"dog","cat"};
        this.dogcatBox = new JComboBox<String>(animal);
        this.dogcatBox.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(dogcatBox, gbc);
    }
    
       
    private void setupNameLabel() {
        GridBagConstraints gbc;
        this.nameLabel = new JLabel("Name:");
        this.nameLabel.setFont(f);
        nameLabel.setHorizontalAlignment(nameLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(nameLabel, gbc);                
    }
    
    
    private void setupNameField() {
        this.nameField = new JTextField("");
        this.nameField.setFont(f);
        nameField.setHorizontalAlignment(nameField.CENTER);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(nameField, gbc);
    }

    
    private void setupColorLabel() {
        GridBagConstraints gbc;
        this.colorLabel = new JLabel("Color:");
        this.colorLabel.setFont(f);
        colorLabel.setHorizontalAlignment(colorLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(colorLabel, gbc);                
    }
    
    
    private void setupColorField() {
        this.colorField = new JTextField("");
        this.colorField.setFont(f);
        colorField.setHorizontalAlignment(colorField.CENTER);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(colorField, gbc);
    }
    
    
    private void setupPriceLabel() {
        GridBagConstraints gbc;
        this.priceLabel = new JLabel("Price:");
        this.priceLabel.setFont(f);
        priceLabel.setHorizontalAlignment(priceLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(priceLabel, gbc);                
    }
    
    
    private void setupPriceField() {
        this.priceField = new JTextField("");
        this.priceField.setFont(f);
        priceField.setHorizontalAlignment(priceField.CENTER);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(priceField, gbc);
    }
    
    
    
    private void setupSoundLabel() {
        GridBagConstraints gbc;
        this.soundLabel = new JLabel("Bark/Meow:");
        this.soundLabel.setFont(f);
        soundLabel.setHorizontalAlignment(soundLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(soundLabel, gbc);                
    }
    
    private void setupSoundBox() {
        String[] sound = new String[] {"yes","no"};
        this.soundBox = new JComboBox<String>(sound);
        this.soundBox.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(soundBox, gbc);
    }
    
    
    private void setupBiteLabel() {
        GridBagConstraints gbc;
        this.biteLabel = new JLabel("Bite/Purr:");
        this.biteLabel.setFont(f);
        biteLabel.setHorizontalAlignment(biteLabel.CENTER);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(biteLabel, gbc);                
    }
    
    private void setupBiteBox() {
        String[] bite = new String[] {"yes","no"};
        this.biteBox = new JComboBox<String>(bite);
        this.biteBox.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(biteBox, gbc);
    }
    
       
    private void setupBuyButton() {
        GridBagConstraints gbc;
        this.buyButton = new JButton("Buy animal");
        this.buyButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(buyButton, gbc);
        
        ActionListener buyAction = b -> {    
            animalController.handleBuyButtonClick();
            };
        buyButton.addActionListener(buyAction);
        
    }
    
    private void setupListButton() {
        GridBagConstraints gbc;
        this.listButton = new JButton("List animals");
        this.listButton.setFont(f);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(listButton, gbc);
        
        ActionListener listAction = l -> {    
            animalController.handleListButtonClick();
            };
        listButton.addActionListener(listAction);
        
    }
    
    
}
