package horseadvertising.view;

import horseadvertising.controller.HorseController;
import java.io.IOException;

public class ConsoleView {
    
    private HorseController horseController;

    public ConsoleView(HorseController horseController) throws IOException {
        this.horseController = horseController;
        action();
    }
    
    
    
    
    public void printMenu() {
        System.out.println("---------------------------------------------------");
        System.out.println("This is a horse advertising program's main menu");
        System.out.println("1. menu: Buy a new horse");
        System.out.println("2. menu: List all horses");
        System.out.println("3. menu: Sell horse by name");
        System.out.println("4. menu: Save sellable horses to a file");
        System.out.println("5. menu: Load sellable horses from a file");
        System.out.println("6. menu: Switch to graphical user interface (GUI)");
        System.out.println("0. menu: Exit");
        System.out.println("---------------------------------------------------");
    }
    
    public void action () throws IOException {
        
        int menu;
        do {
            printMenu();
            menu=extra.Console.readInt("Type in the number of the menu, you want to reach!");
            switch(menu) {
                case 1: horseController.buyHorse();break;
                case 2: horseController.listHorse();break;
                case 3: horseController.sellHorse();break;
                case 4: horseController.saveHorses();break;
                case 5: horseController.loadHorses();break;
                case 6: horseController.swingCreate();break;
            }
        }
        while (menu!=0);
        
    }

    
    
    
    
}
