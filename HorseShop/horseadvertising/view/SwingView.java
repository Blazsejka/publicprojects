
package horseadvertising.view;

import horseadvertising.controller.HorseController;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SwingView {
    
    private HorseController horseController;
    private JFrame frame = new JFrame();
    private JPanel panel;
    private JLabel nameLabel;
    public JTextField nameField;
    private Font font = new Font("Courier New", Font.BOLD, 20);
    public JTextArea screen;
    private JButton buyButton;
    private JLabel ageLabel;
    public JTextField ageField;
    private JButton listButton;
    private JLabel sellLabel;
    public JTextField sellField;
    private JButton sellButton;
    private JLabel genderLabel;
    public JComboBox genderBox;
    private JLabel colorLabel;
    public JComboBox colorBox;
    private JButton saveButton;
    private JButton loadButton;
    

    public SwingView(HorseController horseController) {
        this.horseController=horseController;
        init();
    }
    
    
    private void init() {
        setupWindow();
        setupNameLabel();
        setupNameField();
        setupAgeLabel();
        setupAgeField();
        setupBuyButton();
        setupListButton();
        setupScreenArea();
        setupSellButton();
        setupSellField();
        setupSellLabel();
        setupColorBox();
        setupColorLabel();
        setupGenderBox();
        setupGenderLabel();
        setupSaveButton();
        setupLoadButton();
        frame.setVisible(true);
    }
    
    private void setupWindow() {
        frame.setSize(600,900);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Horse advertising program");
        frame.setLayout(new GridLayout(1, 1));
        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        frame.add(panel);
        GridBagLayout gbl = new GridBagLayout();
        panel.setLayout(gbl);
        frame.setResizable(false);
    } 

    private void setupNameLabel() {
        GridBagConstraints gbc;
        this.nameLabel = new JLabel("Name:");
        nameLabel.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(nameLabel, gbc);                
    }
    
    private void setupNameField() {
        GridBagConstraints gbc;
        this.nameField = new JTextField("");
        nameField.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(nameField, gbc);                
    }
    
    private void setupGenderLabel() {
        GridBagConstraints gbc;
        this.genderLabel = new JLabel("Gender:");
        genderLabel.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(genderLabel, gbc);                
    }
    
    private void setupGenderBox() {
        String[] gender = new String[] {"MARE","STALLION"};
        GridBagConstraints gbc;
        this.genderBox = new JComboBox(gender);
        genderBox.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(genderBox, gbc);                
    }
    
    private void setupColorLabel() {
        GridBagConstraints gbc;
        this.colorLabel = new JLabel("Color:");
        colorLabel.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(colorLabel, gbc);                
    }
    
    private void setupColorBox() {
        String[] color = new String[] {"BLACK", "WHITE","BROWN","PINTO","DAPPLEGREY"};
        GridBagConstraints gbc;
        this.colorBox = new JComboBox(color);
        colorBox.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(colorBox, gbc);                
    }
    
    private void setupAgeLabel() {
        GridBagConstraints gbc;
        this.ageLabel = new JLabel("Age:");
        ageLabel.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(ageLabel, gbc);                
    }
    
    private void setupAgeField() {
        GridBagConstraints gbc;
        this.ageField = new JTextField("");
        ageField.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(ageField, gbc);                
    }
    
    private void setupBuyButton() {
        GridBagConstraints gbc;
        this.buyButton = new JButton("Buy horse");
        this.buyButton.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(buyButton, gbc);
        
        ActionListener buyAction = b -> {    
            horseController.handleBuyButtonClick();
        };
        buyButton.addActionListener(buyAction);
    }    
    
    private void setupListButton() {
        GridBagConstraints gbc;
        this.listButton = new JButton("List horses");
        this.listButton.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(listButton, gbc);
        
        ActionListener listAction = l -> {    
            horseController.handleListButtonClick();
        };
        listButton.addActionListener(listAction);
    }
    
    private void setupScreenArea() {
        GridBagConstraints gbc;
        this.screen = new JTextArea("");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        screen.setLineWrap(true);
        screen.setEditable(false);
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(screen, gbc);                
    }
    
    private void setupSellLabel() {
        GridBagConstraints gbc;
        this.sellLabel = new JLabel("Sell by name:");
        sellLabel.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(sellLabel, gbc);                
    }
    
    private void setupSellField() {
        GridBagConstraints gbc;
        this.sellField = new JTextField("");
        sellField.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(sellField, gbc);                
    }
    
    private void setupSellButton() {
        GridBagConstraints gbc;
        this.sellButton = new JButton("Sell horse");
        this.sellButton.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(sellButton, gbc);
        
        ActionListener sellAction = s -> {    
            horseController.handleSellButtonClick();
        };
        sellButton.addActionListener(sellAction);
    }
    
    private void setupSaveButton() {
        GridBagConstraints gbc;
        this.saveButton = new JButton("Save to an outer file");
        this.saveButton.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(saveButton, gbc);
        
        ActionListener saveAction = s -> {    
            try {
                horseController.handleSaveButtonClick();
            } catch (IOException ex) {
                Logger.getLogger(SwingView.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        saveButton.addActionListener(saveAction);
    }
    
    private void setupLoadButton() {
        GridBagConstraints gbc;
        this.loadButton = new JButton("Load from an outer file");
        this.loadButton.setFont(font);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 10;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(loadButton, gbc);
        
        ActionListener loadAction = l -> {    
            try {
                horseController.handleLoadButtonClick();
            } catch (IOException ex) {
                Logger.getLogger(SwingView.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        loadButton.addActionListener(loadAction);
    }
    
    
}
