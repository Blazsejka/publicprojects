
package horseadvertising.controller;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import horseadvertising.model.Color;
import horseadvertising.model.Horse;
import horseadvertising.view.ConsoleView;
import horseadvertising.view.SwingView;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class HorseController {

    private List<Horse> stable = new ArrayList<>();
    private SwingView gui;
    private ConsoleView cui;
    private String csvPath="D:\\Coding\\HorseAdvertising\\Stable.csv";

    public HorseController() throws IOException {
        upLoad();
        this.cui=new ConsoleView(this);
    }
    
    private void upLoad() {
        stable.add(new Horse("mare", 5, "Kincsem", Color.BROWN));
        stable.add(new Horse("stallion", 4, "Rocket", Color.BLACK));
        stable.add(new Horse("mare", 7, "Tarkusz", Color.PINTO));
        stable.add(new Horse("stallion", 3, "Villám", Color.DAPPLEGREY));
    }
    
    public void buyHorse() {
        Horse tmphorse = new Horse(null, 0, null, null);
        boolean genderok=false;
        do {
            String tmpgender=extra.Console.readLine("Type in the gender of the horse (mare/stallion): ");
            if (tmpgender.equals("mare") || tmpgender.equals("stallion")) {
                tmphorse.setGender(tmpgender);
                genderok=true;
            }
        } while (genderok!=true);
        
        tmphorse.setName(extra.Console.readLine("Type is the name of the horse: "));
        tmphorse.setAge(extra.Console.readInt("Type in the age of the horse: "));
        boolean ok=false;
        do {
            String tmpcolor=extra.Console.readLine("Type in the color of the horse with upper case (black, white, brown, pinto, dapplegrey): ");
            EnumSet<Color> colors = EnumSet.allOf(Color.class);
                for (Color c : colors) {
                    if (c.name().equals(tmpcolor)) {
                        tmphorse.setColor(c);
                        ok=true;
                    } 
                }
        } while (ok==false);
        stable.add(tmphorse);
    }
    
    
    public void listHorse() {
        System.out.println(stable.toString());
    }
    
    
    public void sellHorse() {
        String tmpdelname=extra.Console.readLine("Type in the name of the horse you want to sell: ");
        Horse delhorse=stable.stream().filter(e -> e.getName().equals(tmpdelname)).findFirst().get();
        stable.remove(delhorse);        
    }
    
    
    public void saveHorses() throws IOException {
        try(FileWriter fw = new FileWriter(csvPath,true);
            CSVWriter csvWriter = new CSVWriter(fw)) {
            for (Horse h: stable) {
                String[] nextLine = {h.getName(),h.getGender(),Integer.toString(h.getAge()),h.getColor().name()};
                csvWriter.writeNext(nextLine);
            }   
        }
    }
    
    public void loadHorses() throws FileNotFoundException, IOException {
        try (FileReader fr = new FileReader(csvPath);
            CSVReader csv = new CSVReader(fr)) {
            String[] row;
            while ((row = csv.readNext()) != null) {
                for (String record : row) {
                    System.out.println(record);
                }
            }
        }
    }
    
    public void swingCreate() {
        this.gui=new SwingView(this);   
    }
    
    public void handleBuyButtonClick() {
        Horse tmphorse = new Horse(null, 0, null, null);
        tmphorse.setName(gui.nameField.getText());
        tmphorse.setAge(Integer.parseInt(gui.ageField.getText()));
        
        EnumSet<Color> colors = EnumSet.allOf(Color.class);
            for (Color c : colors) {
                if (c.name().equals(gui.colorBox.getSelectedItem().toString())) {
                    tmphorse.setColor(c);
                }
            }
        
        tmphorse.setGender(gui.genderBox.getSelectedItem().toString());
        stable.add(tmphorse);
    }

    public void handleListButtonClick() {
        gui.screen.setText(stable.toString());
    }

    public void handleSellButtonClick() {
        Horse sellhorse=stable.stream().filter(e -> e.getName().equals(gui.sellField.getText())).findFirst().get();
        stable.remove(sellhorse);
    }

    public void handleSaveButtonClick() throws IOException {
        try(FileWriter fw = new FileWriter(csvPath,true);
            CSVWriter csvWriter = new CSVWriter(fw)) {
            for (Horse h: stable) {
                String[] nextLine = {h.getName(),h.getGender(),Integer.toString(h.getAge()),h.getColor().name()};
                csvWriter.writeNext(nextLine);
            }   
        }
    }

    public void handleLoadButtonClick() throws FileNotFoundException, IOException {
        String all=null;
        try (FileReader fr = new FileReader(csvPath);
            CSVReader csv = new CSVReader(fr)) {
            String[] row;
            while ((row = csv.readNext()) != null) {
                for (String record : row) {
                    all=all + record+" ";
                }   
            }
        }
        gui.screen.setText(all);
    }
    
    
    
}
