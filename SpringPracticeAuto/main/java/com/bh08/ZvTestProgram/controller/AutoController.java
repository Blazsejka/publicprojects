package com.bh08.ZvTestProgram.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bh08.ZvTestProgram.model.Engine;
import com.bh08.ZvTestProgram.service.AutoService;
import com.bh08.ZvTestProgram.viewmodel.AutoFormData;


@Controller
public class AutoController {
	
	@Autowired
	private AutoService autoService;
	
	@RequestMapping(value = "auto", method = RequestMethod.GET)
	public String login(Model model) {
		AutoFormData autoFormData = new AutoFormData();
		model.addAttribute("autoFormData", autoFormData);
		return "AutoGen.html";
	}
	
	@RequestMapping(value = "auto", method = RequestMethod.POST)
	public String login(@ModelAttribute("autoFormData") @Valid AutoFormData autoFormData,Model model) {

		String color = autoFormData.getColor();
		int seats = autoFormData.getSeats();
		int hp = autoFormData.getHp();
		String fuel = autoFormData.getFuel();
		
		System.out.println(hp);
		System.out.println(fuel);
		
		Engine engine = autoService.saveNewEngine(fuel, hp);
		autoService.saveNewAuto(engine, color, seats);
		
		model.addAttribute("autoFormData", autoFormData);
		return "AutoGen.html";
	}
	
	
}
