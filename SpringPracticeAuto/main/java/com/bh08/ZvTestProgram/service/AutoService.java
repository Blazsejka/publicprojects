package com.bh08.ZvTestProgram.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.ZvTestProgram.dao.AutoDao;
import com.bh08.ZvTestProgram.dao.EngineDao;
import com.bh08.ZvTestProgram.model.Auto;
import com.bh08.ZvTestProgram.model.Engine;
import com.bh08.ZvTestProgram.model.Fuel;

@Service
public class AutoService {
	
	@Autowired
	private AutoDao autoDao;
	
	@Autowired
	private EngineDao engineDao;
	
	public void saveNewAuto(Engine engine, String color, int seats) {
		Auto auto = new Auto();
		auto.setEngine(engine);
		auto.setColor(color);
		auto.setSeats(seats);
		autoDao.saveAndFlush(auto);
	}
	
	
	public Engine saveNewEngine(String fuel, int hp) {
		
		Engine engine = new Engine();
		
		if (fuel.equals("petrol")) {
			engine.setFuel(Fuel.PETROL);
		}
		if (fuel.equals("diesel")) {
			engine.setFuel(Fuel.DIESEL);
		}
		if (fuel.equals("electric")) {
			engine.setFuel(Fuel.ELECTRIC);
		}
		
		engine.setHp(hp);
		
		engineDao.saveAndFlush(engine);
		return engine;
	}
	
	
	public Optional<Auto> getAutoById(Long id) {
		return autoDao.findById(id);
	}
	
	
}
