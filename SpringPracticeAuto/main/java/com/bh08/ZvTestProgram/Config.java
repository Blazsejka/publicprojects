package com.bh08.ZvTestProgram;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = "com.bh08.ZvTestProgram")
@EnableJpaRepositories(basePackages = "com.bh08.ZvTestProgram")
@ComponentScan(basePackages = "com.bh08.ZvTestProgram")
@Configuration
public class Config {
	
	
	
	
}
