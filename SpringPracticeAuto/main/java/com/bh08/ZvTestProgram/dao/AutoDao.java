package com.bh08.ZvTestProgram.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bh08.ZvTestProgram.model.Auto;

@Repository
public interface AutoDao extends JpaRepository<Auto, Long>{
	
	public List<Auto> findAll();
	
	public Optional<Auto> findById(Long id);
	
	
	
}
