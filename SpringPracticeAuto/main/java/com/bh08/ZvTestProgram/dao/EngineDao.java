package com.bh08.ZvTestProgram.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bh08.ZvTestProgram.model.Auto;
import com.bh08.ZvTestProgram.model.Engine;
import com.bh08.ZvTestProgram.model.Fuel;

@Repository
public interface EngineDao extends JpaRepository<Engine, Integer>{
	
	public List<Engine> findAll();
	
	public Auto findById(int id);
	
	public List<Engine> findAllByFuel(Fuel fuel);
	
}
