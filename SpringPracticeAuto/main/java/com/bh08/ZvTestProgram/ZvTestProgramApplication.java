package com.bh08.ZvTestProgram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ZvTestProgramApplication {

	public static void main(String[] args) {
		SpringApplication.run(new Class<?>[] {ZvTestProgramApplication.class, Config.class}, args);
	}

}
