package com.bh08.ZvTestProgram.viewmodel;

import com.bh08.ZvTestProgram.model.Auto;
import com.bh08.ZvTestProgram.model.Engine;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class AutoFormData {
	
	private String color;
	
	private int seats;
	
	private String fuel;
	
	private int hp;
	
}
