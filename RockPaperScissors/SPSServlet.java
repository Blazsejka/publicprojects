import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/sps")
public class SPSServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SPSServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int playerGuess = Integer.parseInt(request.getParameter("ez"));
		int pcGuess=(int)(Math.random()*3)+1;
		System.out.println(pcGuess);
		final int STONE=1;
		final int PAPER=2;
		final int SCISSORS=3;
		
		int result;
		
		if (playerGuess==pcGuess) {
			result=0;
		} else if ((playerGuess==STONE && pcGuess==PAPER) || (playerGuess==PAPER && pcGuess==SCISSORS) || (playerGuess==SCISSORS && pcGuess==STONE)) {
			result=1;
		} else {
			result=-1;
		}
		
		
		HttpSession session = request.getSession(true);
		
		Integer playerScore = (Integer)session.getAttribute("playerScore");
		Integer pcScore = (Integer)session.getAttribute("pcScore");
		
		if(playerScore==null) {
			playerScore=0;
			pcScore=0;
		}
		
		if (result==1)
			pcScore++;
		else if (result==-1)
			playerScore++;
		
		session.setAttribute("playerScore", playerScore);
		session.setAttribute("pcScore", pcScore);
		
		request.setAttribute("result", result);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}
	
}
